/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Harsh
 */
class Performance {
    private String GameName;
    private int TotalPlayed,TotalWon,TotalLost,TotalDraw,Rank;
    public Performance(String GameName, int TotalPlayed, int TotalWon, int TotalLost, int TotalDraw, int Rank){
        this.GameName = GameName;
        this.TotalPlayed = TotalPlayed;
        this.TotalWon = TotalWon;
        this.TotalLost = TotalLost;
        this.TotalDraw = TotalDraw;
        this.Rank = Rank;
    }

    public String getGameName(){
        return GameName;
    }
    public int getTotalPlayed(){
        return TotalPlayed;
    }
    public int getTotalWon(){
        return TotalWon;
    }
    public int getTotalLost(){
        return TotalLost;
    }
    public int getTotalDraw(){
        return TotalDraw;
    }
    public int getRank(){
        return Rank;
    }
}
