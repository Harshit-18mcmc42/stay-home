
import AppPackage.AnimationClass;
import jaco.mp3.player.MP3Player;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Harsh
 */
public class SignUpForm extends javax.swing.JFrame {
    AnimationClass ac = new AnimationClass();
    /**
     * Creates new form SignUpForm
     */
    public SignUpForm() {
        initComponents();
        playMusic();
        slideshow();
    }
    
    
    MP3Player mp3player = new MP3Player(getClass().getResource("SignupSound.mp3")); 
    public void playMusic(){
        mp3player.play();
        mp3player.setRepeat(true);
    }
    public void stopMusic(){
        mp3player.pause();
    }
    public void closeMusic(){
        mp3player.stop();
    }
    
    
int n = 0;
    public void slideshow()        
{
    new Thread(new Runnable()
    {
        public void run()
        {
            
            try{
                    while(n!=-1){
                        switch(n){
                            case 0:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(0,-400,12,10, jLabel1);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel2);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel3);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel4);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel5);
                                ac.jLabelXLeft(2000, 1600, 12, 10, jLabel6);
                                ac.jLabelXLeft(2400, 2000, 12, 10, jLabel7);
                                ac.jLabelXLeft(2800, 2400, 12, 10, jLabel8);
                                n++;
                            case 1:
                                Thread.sleep(3000);
                                ac.jLabelXLeft(-400,-800,12,10, jLabel1);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel2);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel3);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel4);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel5);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel6);
                                ac.jLabelXLeft(2000, 1600, 12, 10, jLabel7);
                                ac.jLabelXLeft(2400, 2000, 12, 10, jLabel8);
                                n++;
                            case 2:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-800, -1200,12,10, jLabel1);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel2);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel3);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel4);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel5);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel6);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel7);
                                ac.jLabelXLeft(2000, 1600, 12, 10, jLabel8);
                                n++;
                            case 3:
                                Thread.sleep(3000);
                                ac.jLabelXLeft(-1200, -1600,12,10, jLabel1);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel2);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel3);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel4);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel5);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel6);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel7);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel8);
                                n++;
                            case 4:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-1600, -2000,12,10, jLabel1);
                                ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel2);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel3);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel4);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel5);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel6);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel7);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel8);
                                n++;
                            case 5:
                                Thread.sleep(3000);
                                ac.jLabelXLeft(-2000, -2400,12,10, jLabel1);
                                ac.jLabelXLeft(-1600, -2000, 12, 10, jLabel2);
                                ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel3);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel4);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel5);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel6);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel7);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel8);
                                n++; 
                            case 6:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-2400, -2800,12,10, jLabel1);
                                ac.jLabelXLeft(-2000, -2400, 12, 10, jLabel2);
                                ac.jLabelXLeft(-1600, -2000, 12, 10, jLabel3);
                                ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel4);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel5);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel6);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel7);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel8);
                                n++;    
                            case 7:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-2800, -2400,12,10,jLabel1);
                                ac.jLabelXRight(-2400,-2000, 12, 10, jLabel2);
                                ac.jLabelXRight(-2000,-1600, 12, 10, jLabel3);
                                ac.jLabelXRight(-1600, -1200,12,10,jLabel4);
                                ac.jLabelXRight(-1200,-800, 12, 10, jLabel5);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel6);
                                ac.jLabelXRight(-400,0, 12, 10, jLabel7);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel8);
                                n++;
                            case 8:
                                Thread.sleep(2000);
                                ac.jLabelXRight(-2400, -2000,12,10, jLabel1);
                                ac.jLabelXRight(-2000, -1600, 12, 10, jLabel2);
                                ac.jLabelXRight(-1600, -1200, 12, 10, jLabel3);
                                ac.jLabelXRight(-1200, -800, 12, 10, jLabel4);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel5);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel6);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel7);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel8);
                                n++; 
                            case 9:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-2000, -1600,12,10, jLabel1);
                                ac.jLabelXRight(-1600, -1200, 12, 10, jLabel2);
                                ac.jLabelXRight(-1200, -800, 12, 10, jLabel3);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel4);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel5);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel6);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel7);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel8);
                                n++;   
                            case 10:
                                Thread.sleep(2000);
                                ac.jLabelXRight(-1600, -1200,12,10, jLabel1);
                                ac.jLabelXRight(-1200, -800, 12, 10, jLabel2);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel3);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel4);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel5);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel6);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel7);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel8);
                                n++;
                            case 11:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-1200, -800,12,10, jLabel1);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel2);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel3);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel4);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel5);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel6);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel7);
                                ac.jLabelXRight(1600, 2000, 12, 10, jLabel8);
                                n++;
                            case 12:
                                Thread.sleep(2000);
                                ac.jLabelXRight(-800,-400,12,10, jLabel1);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel2);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel3);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel4);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel5);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel6);
                                ac.jLabelXRight(1600, 2000, 12, 10, jLabel7);
                                ac.jLabelXRight(2000, 2400, 12, 10, jLabel8);
                                n++;
                            case 13:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-400,0,12,10, jLabel1);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel2);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel3);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel4);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel5);
                                ac.jLabelXRight(1600, 2000, 12, 10, jLabel6);
                                ac.jLabelXRight(2000, 2400, 12, 10, jLabel7);
                                ac.jLabelXRight(2400, 2800, 12, 10, jLabel8);
                                n = 0;
                            }
                        }
                    }
                catch(Exception ex)
                {
                    System.out.println(ex);
                }
            }   
    }).start();
}
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        GenderChoice = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        Left = new javax.swing.JPanel();
        UserName = new javax.swing.JLabel();
        UserNameField = new javax.swing.JTextField();
        EmailIDField = new javax.swing.JTextField();
        SendOTP = new javax.swing.JButton();
        MobileNo = new javax.swing.JLabel();
        EmailID = new javax.swing.JLabel();
        MobileNoField = new javax.swing.JTextField();
        EnterOTP = new javax.swing.JLabel();
        OTPField = new javax.swing.JTextField();
        VerifyOTP = new javax.swing.JButton();
        Gender = new javax.swing.JLabel();
        GenderMale = new javax.swing.JRadioButton();
        GenderFemale = new javax.swing.JRadioButton();
        Gender1 = new javax.swing.JLabel();
        Password = new javax.swing.JLabel();
        DateChooser = new datechooser.beans.DateChooserCombo();
        CPassword = new javax.swing.JLabel();
        PasswordField = new javax.swing.JPasswordField();
        CPasswordField = new javax.swing.JPasswordField();
        Register = new javax.swing.JButton();
        Music = new javax.swing.JCheckBox();
        Back = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        Right = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Left.setMinimumSize(new java.awt.Dimension(400, 400));
        Left.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        UserName.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        UserName.setForeground(new java.awt.Color(240, 240, 240));
        UserName.setText("User Name");
        Left.add(UserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 10, -1, 30));

        UserNameField.setBackground(new java.awt.Color(204, 204, 255));
        Left.add(UserNameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 10, 180, 30));
        Left.add(EmailIDField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, 180, 30));

        SendOTP.setText("Send OTP");
        SendOTP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SendOTPActionPerformed(evt);
            }
        });
        Left.add(SendOTP, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 90, 90, 20));

        MobileNo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        MobileNo.setForeground(new java.awt.Color(240, 240, 240));
        MobileNo.setText("Mobile No.");
        Left.add(MobileNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, 100, 20));

        EmailID.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        EmailID.setForeground(new java.awt.Color(240, 240, 240));
        EmailID.setText("EmailID");
        Left.add(EmailID, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, 80, 30));

        MobileNoField.setEnabled(false);
        Left.add(MobileNoField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 160, 170, 30));

        EnterOTP.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        EnterOTP.setForeground(new java.awt.Color(240, 240, 240));
        EnterOTP.setText("Enter OTP");
        Left.add(EnterOTP, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, 100, 30));

        OTPField.setEnabled(false);
        Left.add(OTPField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 110, 90, 30));

        VerifyOTP.setText("Verify OTP");
        VerifyOTP.setEnabled(false);
        VerifyOTP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VerifyOTPActionPerformed(evt);
            }
        });
        Left.add(VerifyOTP, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 120, 90, 20));

        Gender.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Gender.setForeground(new java.awt.Color(240, 240, 240));
        Gender.setText("Gender");
        Left.add(Gender, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 200, 100, 20));

        GenderChoice.add(GenderMale);
        GenderMale.setForeground(new java.awt.Color(51, 255, 0));
        GenderMale.setSelected(true);
        GenderMale.setText("Male");
        GenderMale.setEnabled(false);
        Left.add(GenderMale, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 200, 80, -1));

        GenderChoice.add(GenderFemale);
        GenderFemale.setForeground(new java.awt.Color(153, 255, 0));
        GenderFemale.setText("Female");
        GenderFemale.setEnabled(false);
        Left.add(GenderFemale, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 200, 80, -1));

        Gender1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Gender1.setForeground(new java.awt.Color(240, 240, 240));
        Gender1.setText("Date of Birth");
        Left.add(Gender1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, 120, 20));

        Password.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        Password.setForeground(new java.awt.Color(240, 240, 240));
        Password.setText("Password");
        Left.add(Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 280, 120, 20));

        DateChooser.setCurrentView(new datechooser.view.appearance.AppearancesList("Light",
            new datechooser.view.appearance.ViewAppearance("custom",
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    true,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 255),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(128, 128, 128),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(255, 0, 0),
                    false,
                    false,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                (datechooser.view.BackRenderer)null,
                false,
                true)));
    Left.add(DateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 240, 170, -1));

    CPassword.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
    CPassword.setForeground(new java.awt.Color(240, 240, 240));
    CPassword.setText("Confirm Password");
    Left.add(CPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 310, 180, 20));

    PasswordField.setEnabled(false);
    Left.add(PasswordField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 280, 170, -1));

    CPasswordField.setEnabled(false);
    Left.add(CPasswordField, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 310, 160, -1));

    Register.setBackground(new java.awt.Color(153, 255, 0));
    Register.setForeground(new java.awt.Color(0, 0, 102));
    Register.setText("Register");
    Register.setEnabled(false);
    Register.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            RegisterActionPerformed(evt);
        }
    });
    Left.add(Register, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 360, 90, 30));

    Music.setBackground(new java.awt.Color(0, 204, 204));
    Music.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    Music.setForeground(new java.awt.Color(240, 240, 240));
    Music.setSelected(true);
    Music.setText("Music");
    Music.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            MusicActionPerformed(evt);
        }
    });
    Left.add(Music, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 360, 60, -1));

    Back.setBackground(new java.awt.Color(51, 51, 0));
    Back.setForeground(new java.awt.Color(204, 204, 0));
    Back.setText("Back");
    Back.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            BackActionPerformed(evt);
        }
    });
    Left.add(Back, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 360, 80, 30));

    jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/blank.png"))); // NOI18N
    Left.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

    jPanel1.add(Left, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

    Right.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

    jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/StayHome.png"))); // NOI18N
    jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
    Right.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

    jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Checkers.png"))); // NOI18N
    jLabel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 204, 0), 4, true));
    Right.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 400, 400));

    jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Chess.png"))); // NOI18N
    jLabel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(102, 0, 102), 4, true));
    Right.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 0, 400, 400));

    jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Connect4.png"))); // NOI18N
    jLabel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 51), 4, true));
    Right.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 0, 400, 400));

    jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Dot&Box.png"))); // NOI18N
    jLabel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 255, 0), 4, true));
    Right.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1600, 0, 400, 400));

    jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/HexaPawn.png"))); // NOI18N
    jLabel6.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 0, 153), 4, true));
    Right.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(2000, 0, 400, 400));

    jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Ludo.png"))); // NOI18N
    jLabel7.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 204, 0), 4, true));
    Right.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(2400, 0, 400, 400));

    jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Nim.png"))); // NOI18N
    jLabel8.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 255, 0), 4, true));
    Right.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(2800, 0, 400, 400));

    jPanel1.add(Right, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 400, 400));

    getContentPane().add(jPanel1, "card2");

    pack();
    setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackActionPerformed
        // TODO add your handling code here:
        LogInForm LG = new LogInForm();
        closeMusic();
        setVisible(false);
        LG.setVisible(true);
    }//GEN-LAST:event_BackActionPerformed

    public boolean checkConnection(){
        try{
        Process process = java.lang.Runtime.getRuntime().exec("ping www.google.com"); 
        int x = process.waitFor(); 
        if (x == 0) { 
            return true;
        } 
        else { 
            return false;
        }
        }catch(Exception e){
            System.out.println("Ye hai Error : " + e);
        }
        return false;
    }
    
    
    
    Random rand = new Random();
    int OTP = rand.nextInt(99999999);
    
    private void SendOTPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SendOTPActionPerformed
        // TODO add your handling code here:
    if(checkConnection()){ 
        int flag = 0;
        String email = EmailIDField.getText();
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String userName = UserNameField.getText();

        if(email.matches(regex))
        {
            try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");

            // our SQL SELECT query. 
            // if you only need a few columns, specify them by name instead of using "*"
            String query = "SELECT  emailId FROM registration";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next())
            {
                String Email = rs.getString("emailId");

                if(Email.equals(email))
                {
                    JOptionPane.showMessageDialog(null, "Sorry.. The Email Id is already registered.");
                    flag = 1;
                    break;
                }
            }
                st.close();
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception" + e);
            }
            
            if(flag == 0)
            {
            
                String from = "harshkum2k19@gmail.com";
                String username = "harshkum2k19@gmail.com";
                Properties properties = new Properties();
                properties.put("mail.smtp.auth","true");
                properties.put("mail.smtp.starttls.enable","true");
                properties.put("mail.smtp.host","smtp.gmail.com");
                properties.put("mail.smtp.port",587);
                String password = "Harshkumharshkum";
                Session session = Session.getInstance(properties, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
                try {
                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(from));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
                    message.setSubject("Welcome to MCA Family");
                    
                    
                    
                    message.setText("Hi.. " + userName + " Welcome to the First step to for Registering into very new Game Collection Application... the OTP for the Registration No " + userName + " is --->  "   + OTP + " Please verify your emeial by entering the OTP.                           Thank You..... From MCA Family.");
                    Transport.send(message);
                    JOptionPane.showMessageDialog(null, "Message Sent successfully...");
                    OTPField.setEnabled(true);
                    VerifyOTP.setEnabled(true);

                }catch(MessagingException mex)
                {
                    JOptionPane.showMessageDialog(null, "Error message " + mex);
                }
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Please provide the valid email Id");
        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_SendOTPActionPerformed

    private void VerifyOTPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VerifyOTPActionPerformed
        // TODO add your handling code here:
        if(OTP == Integer.parseInt(OTPField.getText()))
        {
            OTPField.setEnabled(false);
            VerifyOTP.setText("Verified");
            MobileNoField.setEnabled(true);
            GenderMale.setEnabled(true);
            GenderFemale.setEnabled(true);
            DateChooser.setEnabled(true);
            Register.setEnabled(true);
            PasswordField.setEnabled(true);
            CPasswordField.setEnabled(true);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Sorry.. OTP is not Correct..Please enter the correct OTP");
            OTPField.setText("");
        }

    }//GEN-LAST:event_VerifyOTPActionPerformed

    private void RegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegisterActionPerformed
        // TODO add your handling code here:
    if(checkConnection()){
        int flag = 1;
        if(!(PasswordField.getText().equals(CPasswordField.getText())))
        {
            JOptionPane.showMessageDialog(null, "Password and Confirm Password is not mateched..");
            PasswordField.setText("");
            CPasswordField.setText("");
            flag = 0;
        }else if(MobileNoField.getText().length() != 10){
            JOptionPane.showMessageDialog(null, "Please enter valid mobile no.");
            MobileNoField.setText("");
            flag = 0;
        }else if(flag == 1){
            try
            {
                // create a mysql database connection
                String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
                String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
                Class.forName(myDriver);
                Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");

                // create a sql date object so we can use it in our INSERT statement

                // the mysql insert statement
                String query = " insert into Registration (userName, emailId, passWord, mobileNo, gender, dateOfBirth)"
                  + " values (?, ?, ?, ?, ?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                String Name = UserNameField.getText();
                String EmailId = EmailIDField.getText();
                String Password = PasswordField.getText();
                String Gender = "";
                if(GenderMale.isSelected())
                {
                    Gender = "Male";
                }
                if(GenderFemale.isSelected())
                {
                    Gender = "Female";
                }

                String DOB = DateChooser.getText();

                String MobileNo = (MobileNoField.getText());

                preparedStmt.setString (1, Name);
                preparedStmt.setString (2, EmailId);
                preparedStmt.setString (3, Password);
                preparedStmt.setString (4, MobileNo);
                preparedStmt.setString (5, Gender);
                preparedStmt.setString (6, DOB);

                // execute the preparedstatement
                preparedStmt.execute();

                conn.close();
                JOptionPane.showMessageDialog(null, "Thank you for Registering .. Now you can login to get into.");
                LogInForm LogIn = new LogInForm();
                closeMusic();
                setVisible(false);
                LogIn.setVisible(true);
                CreateGameProfile(EmailIDField.getText());
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an Exception");
                JOptionPane.showMessageDialog(null, e);
            }

        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_RegisterActionPerformed

    private void MusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MusicActionPerformed
        // TODO add your handling code here:
        if(Music.isSelected() == true){
            playMusic();
        }else{
            stopMusic();
        }
    }//GEN-LAST:event_MusicActionPerformed

    
    public void CreateGameProfile(String email){
            try
            {
                // create a mysql database connection
                String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
                String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
                Class.forName(myDriver);
                Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");

                // create a sql date object so we can use it in our INSERT statement

                // the mysql insert statement
                String query = " insert into usergameprofile (Email, GameName, TotalPlayed, TotalWon, TotalLost, TotalDraw, Rank)"
                  + " values (?, ?, ?, ?, ?, ?, ?)";

                // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                
                String Game[] = {"Stay Home","ChasKundi","ConnectFour","Dot&Box","Chess","HexaPawn","Ludo","Nim"};
                int score = 0;
                
                for(int i = 0; i < 8; i++){
                    preparedStmt.setString (1, email);
                    preparedStmt.setString (2, Game[i]);
                    preparedStmt.setInt(3, score);
                    preparedStmt.setInt(4, score);
                    preparedStmt.setInt(5, score);
                    preparedStmt.setInt(6, score);
                    preparedStmt.setInt(7, score);
                    
                    preparedStmt.execute();
                }
                
                // execute the preparedstatement

                conn.close();
                
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an Exception");
                JOptionPane.showMessageDialog(null, e);
            }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SignUpForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SignUpForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SignUpForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SignUpForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SignUpForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Back;
    private javax.swing.JLabel CPassword;
    private javax.swing.JPasswordField CPasswordField;
    private datechooser.beans.DateChooserCombo DateChooser;
    private javax.swing.JLabel EmailID;
    private javax.swing.JTextField EmailIDField;
    private javax.swing.JLabel EnterOTP;
    private javax.swing.JLabel Gender;
    private javax.swing.JLabel Gender1;
    private javax.swing.ButtonGroup GenderChoice;
    private javax.swing.JRadioButton GenderFemale;
    private javax.swing.JRadioButton GenderMale;
    private javax.swing.JPanel Left;
    private javax.swing.JLabel MobileNo;
    private javax.swing.JTextField MobileNoField;
    private javax.swing.JCheckBox Music;
    private javax.swing.JTextField OTPField;
    private javax.swing.JLabel Password;
    private javax.swing.JPasswordField PasswordField;
    private javax.swing.JButton Register;
    private javax.swing.JPanel Right;
    private javax.swing.JButton SendOTP;
    private javax.swing.JLabel UserName;
    private javax.swing.JTextField UserNameField;
    private javax.swing.JButton VerifyOTP;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
