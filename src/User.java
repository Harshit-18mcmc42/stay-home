/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Harsh
 */
class User {
    private String status;
    private String name, email;
    public User(String name , String email, String status){
        this.name = name;
        this.email = email;
        this.status = status;
    }
    public String getname(){
        return name;
    }
    public String getemail(){
        return email;
    }
    public String getstatus(){
        return status;
    }
}
