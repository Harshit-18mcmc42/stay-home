
import jaco.mp3.player.MP3Player;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Harsh
 */
public class Home_Frame extends javax.swing.JFrame {

    /**
     * Creates new form Home_Frame
     */
//    public Home_Frame() {
//        initComponents();
//        playMusic();
//        ProfileBtn.doClick();
//    }
    String globalemail = "";
    public Home_Frame(String email)
    {
        globalemail = email;
        initComponents();
        
        try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");

            // our SQL SELECT query. 
            // if you only need a few columns, specify them by name instead of using "*"
            String query = "SELECT * FROM registration";
//            String query1 = "SELECT * FROM batch2k18";
//            Statement st1 = conn.createStatement();
//            ResultSet rs1 = st1.executeQuery(query1);
            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);


            // iterate through the java resultset
            while (rs.next())
            {
                String EmailId = rs.getString("emailId");

                if(EmailId.equals(email))
                {
                     
                      String name =  rs.getString("userName");
                      String halfName = name.substring(1, name.length());
                      char N = name.charAt(0);
                      System.out.println(N);
                    switch (N) {
                        case 'A':
                        case 'a':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("1.png")));
                            break;
                        case 'B':
                        case 'b':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("2.png")));
                            break;
                        case 'C':
                        case 'c':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("3.png")));
                            break;
                        case 'D':
                        case 'd':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("4.png")));
                            break;
                        case 'E':
                        case 'e':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("5.png")));
                            break;
                        case 'F':
                        case 'f':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("6.png")));
                            break;
                        case 'G':
                        case 'g':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("7.png")));
                            break;
                        case 'H':
                        case 'h':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("8.png")));
                            break;
                        case 'I':
                        case 'i':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("9.png")));
                            break;
                        case 'J':
                        case 'j':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("10.png")));
                            break;
                        case 'K':
                        case 'k':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("11.png")));
                            break;
                        case 'L':
                        case 'l':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("12.png")));
                            break;
                        case 'M':
                        case 'm':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("13.png")));
                            break;
                        case 'N':
                        case 'n':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("14.png")));
                            break;
                        case 'O':
                        case 'o':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("15.png")));
                            break;
                        case 'P':
                        case 'p':
                                NameLabel.setIcon(new ImageIcon(getClass().getResource("16.png")));
                                break;
                        case 'Q':
                        case 'q':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("17.png")));
                            break;
                        case 'R':
                        case 'r':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("18.png")));
                            break;
                        case 'S':
                        case 's':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("19.png")));
                            break;
                        case 'T':
                        case 't':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("20.png")));
                            break;
                        case 'U':
                        case 'u':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("21.png")));
                            break;
                        case 'V':
                        case 'v':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("22.png")));
                            break;
                        case 'W':
                        case 'w':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("23.png")));
                            break;
                        case 'X':
                        case 'x':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("24.png")));
                            break;
                        case 'Y':
                        case 'y':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("25.png")));
                            break;
                        case 'Z':
                        case 'z':
                            NameLabel.setIcon(new ImageIcon(getClass().getResource("26.png")));
                            break;
                        default:
                            break;
                    }
                      
                      Name.setText(halfName);
                      
                      // Personal Information 
                      String profileName =  rs.getString("userName");
                      String profileEmail =  rs.getString("emailId");
                      String profileMobile =  rs.getString("mobileNo");
                      String profileGender =  rs.getString("gender");
                      String profileDOB =  rs.getString("dateOfBirth");
                      if(profileGender.equals("Male")){
                          profileGender = "M";
                      }else{
                          profileGender = "F";
                      }
                      
                      ProfileName.setText(profileName);
                      ProfileEmail.setText(profileEmail);
                      ProfileMobile.setText(profileMobile);
                      ProfileGender.setText(profileGender);
                      ProfileDOB.setText(profileDOB);
                      
                      
                }
            }
            rs.close();
            

            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception ye hai   " + e);
            }
            
            playMusic();
            ProfileBtn.doClick();
    }
    
    MP3Player mp3player = new MP3Player(getClass().getResource("EntrySound.mp3")); 
    public void playMusic(){
        mp3player.play();
        mp3player.setRepeat(true);
    }
    public void stopMusic(){
        mp3player.pause();
    }
    public void closeMusic(){
        mp3player.stop();
    }
    
    
    public boolean checkConnection(){
        try{
        Process process = java.lang.Runtime.getRuntime().exec("ping www.google.com"); 
        int x = process.waitFor(); 
        if (x == 0) { 
            return true;
        } 
        else { 
            return false;
        }
        }catch(Exception e){
            System.out.println("Ye hai Error : " + e);
        }
        return false;
    }
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        OnlineBtn = new javax.swing.JButton();
        GameBtn = new javax.swing.JButton();
        ProfileBtn = new javax.swing.JButton();
        Name = new javax.swing.JLabel();
        NameLabel = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton9 = new javax.swing.JButton();
        Music = new javax.swing.JCheckBox();
        Game_Panel = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Profile_Panel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        ProfileEmail = new javax.swing.JLabel();
        ProfileDOB = new javax.swing.JLabel();
        ProfileGender = new javax.swing.JLabel();
        ProfileMobile = new javax.swing.JLabel();
        ProfileName = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TopPlayer = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        PerformanceList = new javax.swing.JTable();
        Online_Panel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        OnlineTable = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        RequestEmail = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(0, 51, 51));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 51), 4, true));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        OnlineBtn.setText("Online");
        OnlineBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OnlineBtnActionPerformed(evt);
            }
        });
        jPanel2.add(OnlineBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 30, -1, 30));

        GameBtn.setText("Games");
        GameBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GameBtnActionPerformed(evt);
            }
        });
        jPanel2.add(GameBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, -1, 30));

        ProfileBtn.setText("Profile");
        ProfileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProfileBtnActionPerformed(evt);
            }
        });
        jPanel2.add(ProfileBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 30, -1, 30));

        Name.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        Name.setForeground(new java.awt.Color(240, 240, 240));
        Name.setText("arshit");
        jPanel2.add(Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 30, 100, 40));

        NameLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/one.png"))); // NOI18N
        jPanel2.add(NameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 30, 40, 40));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo1.png"))); // NOI18N
        jLabel7.setText("jLabel6");
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 70));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(153, 255, 0));
        jLabel9.setText("Welcome ");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, 90, 20));

        jButton9.setText("Log Out");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 20, -1, 40));

        Music.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Music.setForeground(new java.awt.Color(204, 255, 204));
        Music.setSelected(true);
        Music.setText("Music");
        Music.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MusicActionPerformed(evt);
            }
        });
        jPanel2.add(Music, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 60, -1));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 90));

        Game_Panel.setBackground(new java.awt.Color(153, 255, 255));
        Game_Panel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 5, true));
        Game_Panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setBackground(new java.awt.Color(204, 255, 0));
        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton1.setText("Play");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        Game_Panel.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 210, 80, 40));

        jButton2.setBackground(new java.awt.Color(204, 255, 0));
        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton2.setText("Play");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        Game_Panel.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 80, 40));

        jButton3.setBackground(new java.awt.Color(255, 255, 0));
        jButton3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton3.setText("Play");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        Game_Panel.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 80, 40));

        jButton4.setBackground(new java.awt.Color(51, 204, 0));
        jButton4.setFont(new java.awt.Font("Trebuchet MS", 1, 24)); // NOI18N
        jButton4.setText("PLAY..");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        Game_Panel.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 340, -1, 40));

        jButton5.setBackground(new java.awt.Color(255, 255, 0));
        jButton5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton5.setText("Play");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        Game_Panel.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 180, 80, 40));

        jLabel1.setBackground(new java.awt.Color(0, 255, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/GameTwo.png"))); // NOI18N
        jLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(3, 3, 3, 3, new java.awt.Color(51, 0, 153)), "HexaPawn", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Tahoma", 3, 24), new java.awt.Color(102, 0, 102))); // NOI18N
        Game_Panel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 218, 200, 200));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/GameFive.png"))); // NOI18N
        jLabel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(3, 3, 3, 3, new java.awt.Color(0, 0, 0)), "# STAY HOME  #", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Cambria", 3, 36))); // NOI18N
        Game_Panel.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 30, 364, 320));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/GameThree.png"))); // NOI18N
        jLabel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(3, 3, 3, 3, new java.awt.Color(0, 0, 153)), "Ludo", javax.swing.border.TitledBorder.TRAILING, javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Tahoma", 3, 24), new java.awt.Color(0, 0, 102))); // NOI18N
        Game_Panel.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(588, 228, 200, 190));

        jLabel4.setBackground(new java.awt.Color(0, 255, 204));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/chaskundi.png"))); // NOI18N
        jLabel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(3, 3, 3, 3, new java.awt.Color(102, 0, 102)), "ChasKundi", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 3, 24), new java.awt.Color(51, 0, 102))); // NOI18N
        Game_Panel.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 2, 200, 200));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/GameFour.png"))); // NOI18N
        jLabel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(3, 3, 3, 3, new java.awt.Color(102, 0, 102)), "Nim", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 3, 24), new java.awt.Color(102, 0, 102))); // NOI18N
        Game_Panel.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 0, 200, 200));

        getContentPane().add(Game_Panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 800, 430));

        Profile_Panel.setBackground(new java.awt.Color(153, 255, 204));
        Profile_Panel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 5, true));
        Profile_Panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 0), 3, true), "Personal Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 0, 102));
        jLabel6.setText("DOB :");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 110, 60, 30));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(102, 0, 102));
        jLabel8.setText("Name :");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 70, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(102, 0, 102));
        jLabel10.setText("Email :");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 70, 30));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 0, 102));
        jLabel11.setText("Mobile No. :");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 120, 30));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(102, 0, 102));
        jLabel12.setText("Gender :");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 30, 80, 20));

        ProfileEmail.setFont(new java.awt.Font("Traditional Arabic", 1, 18)); // NOI18N
        ProfileEmail.setForeground(new java.awt.Color(51, 51, 0));
        ProfileEmail.setText("harshkum313@gmail.com");
        jPanel1.add(ProfileEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 70, 270, 30));

        ProfileDOB.setFont(new java.awt.Font("Vani", 1, 14)); // NOI18N
        ProfileDOB.setForeground(new java.awt.Color(51, 51, 0));
        ProfileDOB.setText("10/09/1998");
        jPanel1.add(ProfileDOB, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, -1, 40));

        ProfileGender.setFont(new java.awt.Font("Traditional Arabic", 1, 24)); // NOI18N
        ProfileGender.setForeground(new java.awt.Color(51, 51, 0));
        ProfileGender.setText("M");
        jPanel1.add(ProfileGender, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 30, 30, 30));

        ProfileMobile.setFont(new java.awt.Font("Vani", 1, 14)); // NOI18N
        ProfileMobile.setForeground(new java.awt.Color(51, 51, 0));
        ProfileMobile.setText("8299136658");
        jPanel1.add(ProfileMobile, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 120, 120, 20));

        ProfileName.setFont(new java.awt.Font("Traditional Arabic", 1, 18)); // NOI18N
        ProfileName.setForeground(new java.awt.Color(51, 51, 0));
        ProfileName.setText("Harshit");
        jPanel1.add(ProfileName, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 30, 130, 30));

        Profile_Panel.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 380, 170));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 0), 3, true), "Top Players", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18))); // NOI18N
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TopPlayer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Rank", "Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TopPlayer.setFocusable(false);
        jScrollPane2.setViewportView(TopPlayer);

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 340, 120));

        Profile_Panel.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 10, 380, 170));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(102, 0, 102), 3, true), "Performance List", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 24))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PerformanceList.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));
        PerformanceList.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        PerformanceList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Game", "Total Played", "Total Won", "Total Lost", "Total Draw"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(PerformanceList);
        if (PerformanceList.getColumnModel().getColumnCount() > 0) {
            PerformanceList.getColumnModel().getColumn(0).setHeaderValue("Game");
            PerformanceList.getColumnModel().getColumn(1).setHeaderValue("Total Played");
            PerformanceList.getColumnModel().getColumn(2).setHeaderValue("Total Won");
            PerformanceList.getColumnModel().getColumn(3).setHeaderValue("Total Lost");
            PerformanceList.getColumnModel().getColumn(4).setHeaderValue("Total Draw");
        }

        jPanel4.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 740, 160));

        Profile_Panel.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 780, 230));

        getContentPane().add(Profile_Panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 800, 430));

        Online_Panel.setBackground(new java.awt.Color(153, 255, 204));
        Online_Panel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 5, true));
        Online_Panel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 0, 51), 4, true), "Online Player", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Traditional Arabic", 1, 24), new java.awt.Color(51, 51, 0))); // NOI18N
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        OnlineTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Email ID", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        OnlineTable.setEnabled(false);
        OnlineTable.setFocusable(false);
        jScrollPane3.setViewportView(OnlineTable);

        jPanel5.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 60, 720, 210));

        jButton6.setBackground(new java.awt.Color(51, 102, 0));
        jButton6.setFont(new java.awt.Font("Trebuchet MS", 3, 14)); // NOI18N
        jButton6.setForeground(new java.awt.Color(204, 204, 0));
        jButton6.setText("Refresh");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(641, 23, 90, 30));

        Online_Panel.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 760, 290));

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 0), 4, true), "Send Request To Play", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Traditional Arabic", 1, 24), new java.awt.Color(51, 0, 51))); // NOI18N
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        RequestEmail.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        RequestEmail.setText("Email ID :");
        jPanel6.add(RequestEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 70, 30));
        jPanel6.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, 360, 20));

        jButton10.setText("Play Request");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel6.add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 50, -1, -1));

        jButton11.setText("Friend Request");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel6.add(jButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 50, 140, -1));

        Online_Panel.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 320, 760, 90));

        getContentPane().add(Online_Panel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 800, 430));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void GameBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GameBtnActionPerformed
        // TODO add your handling code here:
        Profile_Panel.setVisible(false);
        Online_Panel.setVisible(false);
        Game_Panel.setVisible(true);
    }//GEN-LAST:event_GameBtnActionPerformed
int Remove1 = 0;
int Remove2 = 0;
    private void ProfileBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProfileBtnActionPerformed
        // TODO add your handling code here:
    //if(checkConnection()){
    if(true){
        Profile_Panel.setVisible(true);
        Online_Panel.setVisible(false);
        Game_Panel.setVisible(false);
        if(Remove1 == 1){
            clearPerformance();
            show_performance();
        }else{
            show_performance();
            Remove1 = 1;
        }
        if(Remove2 == 1){
            clearRank();
            show_rank();
        }else{
            show_rank();
            Remove2 = 1;
        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
        
    }//GEN-LAST:event_ProfileBtnActionPerformed

    
    public ArrayList<Performance> performance() {
        ArrayList<Performance> performance = new ArrayList<>();
            try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");
            String query1 = "SELECT * FROM usergameprofile";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query1);
            Performance per;
            while(rs.next()){
                if(rs.getString("Email").equals(globalemail)){
                    per = new Performance(rs.getString("GameName") ,rs.getInt("TotalPlayed"), rs.getInt("TotalWon"),rs.getInt("TotalLost"),rs.getInt("TotalDraw"),rs.getInt("Rank"));
                    performance.add(per);
                }else{
                    System.out.println("Ye nahi mila");
                    System.out.println("Ye hai ::: " + globalemail);
                }
            }
            }catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception" + e);
            }
        return performance;
            
    }
    
    
    
    public ArrayList<TopRank> toprank() {
        ArrayList<TopRank> toprank = new ArrayList<>();
            try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");
            String query1 = "SELECT * FROM registration ORDER By Rank DESC";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query1);
            TopRank rank;
            while(rs.next()){
                rank = new TopRank(rs.getString("userName"),rs.getInt("Rank"));
                toprank.add(rank);
            }
            }catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception" + e);
            }
        return toprank;     
    }
    
    public void show_rank(){
        ArrayList<TopRank> rank = toprank();
        DefaultTableModel model = (DefaultTableModel)TopPlayer.getModel();
        Object[] row = new Object[6];
        for(int i = 0; i < rank.size(); i++){
            row[0] = rank.get(i).getRank();;
            row[1] = rank.get(i).getName();
            model.addRow(row);
        }
    }
    
    public void show_performance(){
        ArrayList<Performance> per = performance();
        DefaultTableModel model = (DefaultTableModel)PerformanceList.getModel();
        Object[] row = new Object[6];
        for(int i = 0; i < per.size(); i++){
            row[0] = per.get(i).getGameName();
            row[1] = per.get(i).getTotalPlayed();
            row[2] = per.get(i).getTotalWon();
            row[3] = per.get(i).getTotalLost();
            row[4] = per.get(i).getTotalDraw();
            row[5] = per.get(i).getRank();
            model.addRow(row);
        }
    }
    
    int remove = 0;
    
    private void OnlineBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OnlineBtnActionPerformed
        // TODO add your handling code here:
    if(checkConnection()){
        Profile_Panel.setVisible(false);
        Game_Panel.setVisible(false);
        Online_Panel.setVisible(true);
        if(remove == 1){
            clearTable();
            show_user();
        }else{
            show_user();
            remove = 1;
        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_OnlineBtnActionPerformed

    
    public void clearTable(){
        OnlineTable.setModel(new DefaultTableModel(null,new String[] {"Name","Email Id", "Status"}));
    }
    public void clearPerformance(){
        PerformanceList.setModel(new DefaultTableModel(null,new String[] {"Game","Total Played", "Total Won", "Total Lost", "Total Draw", "Rank"}));
    }
    public void clearRank(){
        TopPlayer.setModel(new DefaultTableModel(null,new String[] {"Name","Rank"}));
    }
    

    
    public ArrayList<User> userList() {
        ArrayList<User> userList = new ArrayList<>();
            try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");
            String query1 = "SELECT userName, emailId, Online FROM registration";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query1);
            User user;
            while(rs.next()){
                String status = "";
                if(rs.getInt("Online") == 0){
                    status = "Offline";
                }else if(rs.getInt("Online") == 1){
                    status = "Online";
                }else if(rs.getInt("Online") == 2){
                    status = "Playing";
                }
                user = new User(rs.getString("userName") ,rs.getString("emailId") ,status);
                userList.add(user);
            }
            }catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception" + e);
            }
        return userList;
            
    }
    
    public void show_user(){
        ArrayList<User> list = userList();
        DefaultTableModel model = (DefaultTableModel)OnlineTable.getModel();
        String[] row = new String[3];
        for(int i = 0; i < list.size(); i++){
            row[0] = list.get(i).getname();
            row[1] = list.get(i).getemail();
            row[2] = list.get(i).getstatus();
            model.addRow(row);
        }
    }
    
    
    
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        //if(checkConnection()){
        if(true){
            Board board = new Board(globalemail);
            closeMusic();
            setVisible(false);
            board.setVisible(true);
        }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
            try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
                try (Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123")) {
                    String qupdate = " update registration set Online = ? where emailId = ?";
                    PreparedStatement preparedStmt = conn.prepareStatement(qupdate);
                    int online = 0;
                    preparedStmt.setInt(1, online);
                    preparedStmt.setString(2, globalemail);
                    preparedStmt.executeUpdate();
                    JOptionPane.showMessageDialog(null, "You are offline Now");
                }
            }catch (HeadlessException | ClassNotFoundException | SQLException e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception    " + e);
            }
        LogInForm LG = new LogInForm();
        closeMusic();
        setVisible(false);
        LG.setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void MusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MusicActionPerformed
        if(Music.isSelected() == true){
            playMusic();
        }else{
            stopMusic();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_MusicActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
            Chaskundi cs = new Chaskundi(globalemail);
            closeMusic();
            setVisible(false);
            cs.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Abhi Bana nahi hai..");
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Abhi Bana nahi hai..");
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Abhi Bana nahi hai..");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
            clearTable();
            show_user();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        // TODO add your handling code here:
        Introducton In = new Introducton();
        In.setVisible(true);
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Under Construction ... Wait for version 2.0");
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Under Construction ... Wait for version 2.0");
    }//GEN-LAST:event_jButton11ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home_Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home_Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home_Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home_Frame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new Home_Frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton GameBtn;
    private javax.swing.JPanel Game_Panel;
    private javax.swing.JCheckBox Music;
    private javax.swing.JLabel Name;
    private javax.swing.JLabel NameLabel;
    private javax.swing.JButton OnlineBtn;
    private javax.swing.JTable OnlineTable;
    private javax.swing.JPanel Online_Panel;
    private javax.swing.JTable PerformanceList;
    private javax.swing.JButton ProfileBtn;
    private javax.swing.JLabel ProfileDOB;
    private javax.swing.JLabel ProfileEmail;
    private javax.swing.JLabel ProfileGender;
    private javax.swing.JLabel ProfileMobile;
    private javax.swing.JLabel ProfileName;
    private javax.swing.JPanel Profile_Panel;
    private javax.swing.JLabel RequestEmail;
    private javax.swing.JTable TopPlayer;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
