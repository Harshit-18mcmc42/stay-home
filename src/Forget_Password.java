
import AppPackage.AnimationClass;
import jaco.mp3.player.MP3Player;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author harsh_000
 */
public class Forget_Password extends javax.swing.JFrame {
    AnimationClass ac = new AnimationClass();

    /**
     * Creates new form Forget_Password
     */
    public Forget_Password() {
        initComponents();
        playMusic();
        slideshow();
    }
    
    
    MP3Player mp3player = new MP3Player(getClass().getResource("SignupSound.mp3")); 
    public void playMusic(){
        mp3player.play();
        mp3player.setRepeat(true);
    }
    public void stopMusic(){
        mp3player.pause();
    }
    public void closeMusic(){
        mp3player.stop();
    }

    int n = 0;
    public void slideshow()        
{
    new Thread(new Runnable()
    {
        public void run()
        {
            
            try{
                while(n!=-1){
                    switch(n){
                        case 0:
                            Thread.sleep(2000);
                            ac.jLabelXLeft(0,-400,12,10, jLabel1);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel2);
                            ac.jLabelXLeft(800, 400, 12, 10, jLabel3);
                            ac.jLabelXLeft(1200, 800, 12, 10, jLabel4);
                            ac.jLabelXLeft(1600, 1200, 12, 10, jLabel5);
                            ac.jLabelXLeft(2000, 1600, 12, 10, jLabel6);
                            ac.jLabelXLeft(2400, 2000, 12, 10, jLabel7);
                            ac.jLabelXLeft(2800, 2400, 12, 10, jLabel8);
                            n++;
                        case 1:
                            Thread.sleep(3000);
                            ac.jLabelXLeft(-400,-800,12,10, jLabel1);
                            ac.jLabelXLeft(0, -400, 12, 10, jLabel2);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel3);
                            ac.jLabelXLeft(800, 400, 12, 10, jLabel4);
                            ac.jLabelXLeft(1200, 800, 12, 10, jLabel5);
                            ac.jLabelXLeft(1600, 1200, 12, 10, jLabel6);
                            ac.jLabelXLeft(2000, 1600, 12, 10, jLabel7);
                            ac.jLabelXLeft(2400, 2000, 12, 10, jLabel8);
                            n++;
                        case 2:
                            Thread.sleep(2000);
                            ac.jLabelXLeft(-800, -1200,12,10, jLabel1);
                            ac.jLabelXLeft(-400, -800, 12, 10, jLabel2);
                            ac.jLabelXLeft(0, -400, 12, 10, jLabel3);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel4);
                            ac.jLabelXLeft(800, 400, 12, 10, jLabel5);
                            ac.jLabelXLeft(1200, 800, 12, 10, jLabel6);
                            ac.jLabelXLeft(1600, 1200, 12, 10, jLabel7);
                            ac.jLabelXLeft(2000, 1600, 12, 10, jLabel8);
                            n++;
                        case 3:
                            Thread.sleep(3000);
                            ac.jLabelXLeft(-1200, -1600,12,10, jLabel1);
                            ac.jLabelXLeft(-800, -1200, 12, 10, jLabel2);
                            ac.jLabelXLeft(-400, -800, 12, 10, jLabel3);
                            ac.jLabelXLeft(0, -400, 12, 10, jLabel4);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel5);
                            ac.jLabelXLeft(800, 400, 12, 10, jLabel6);
                            ac.jLabelXLeft(1200, 800, 12, 10, jLabel7);
                            ac.jLabelXLeft(1600, 1200, 12, 10, jLabel8);
                            n++;
                        case 4:
                            Thread.sleep(2000);
                            ac.jLabelXLeft(-1600, -2000,12,10, jLabel1);
                            ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel2);
                            ac.jLabelXLeft(-800, -1200, 12, 10, jLabel3);
                            ac.jLabelXLeft(-400, -800, 12, 10, jLabel4);
                            ac.jLabelXLeft(0, -400, 12, 10, jLabel5);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel6);
                            ac.jLabelXLeft(800, 400, 12, 10, jLabel7);
                            ac.jLabelXLeft(1200, 800, 12, 10, jLabel8);
                            n++;
                        case 5:
                            Thread.sleep(3000);
                            ac.jLabelXLeft(-2000, -2400,12,10, jLabel1);
                            ac.jLabelXLeft(-1600, -2000, 12, 10, jLabel2);
                            ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel3);
                            ac.jLabelXLeft(-800, -1200, 12, 10, jLabel4);
                            ac.jLabelXLeft(-400, -800, 12, 10, jLabel5);
                            ac.jLabelXLeft(0, -400, 12, 10, jLabel6);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel7);
                            ac.jLabelXLeft(800, 400, 12, 10, jLabel8);
                            n++; 
                        case 6:
                            Thread.sleep(2000);
                            ac.jLabelXLeft(-2400, -2800,12,10, jLabel1);
                            ac.jLabelXLeft(-2000, -2400, 12, 10, jLabel2);
                            ac.jLabelXLeft(-1600, -2000, 12, 10, jLabel3);
                            ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel4);
                            ac.jLabelXLeft(-800, -1200, 12, 10, jLabel5);
                            ac.jLabelXLeft(-400, -800, 12, 10, jLabel6);
                            ac.jLabelXLeft(0, -400, 12, 10, jLabel7);
                            ac.jLabelXLeft(400, 0, 12, 10, jLabel8);
                            n++;    
                        case 7:
                            Thread.sleep(3000);
                            ac.jLabelXRight(-2800, -2400,12,10,jLabel1);
                            ac.jLabelXRight(-2400,-2000, 12, 10, jLabel2);
                            ac.jLabelXRight(-2000,-1600, 12, 10, jLabel3);
                            ac.jLabelXRight(-1600, -1200,12,10,jLabel4);
                            ac.jLabelXRight(-1200,-800, 12, 10, jLabel5);
                            ac.jLabelXRight(-800, -400, 12, 10, jLabel6);
                            ac.jLabelXRight(-400,0, 12, 10, jLabel7);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel8);
                            n++;
                        case 8:
                            Thread.sleep(2000);
                            ac.jLabelXRight(-2400, -2000,12,10, jLabel1);
                            ac.jLabelXRight(-2000, -1600, 12, 10, jLabel2);
                            ac.jLabelXRight(-1600, -1200, 12, 10, jLabel3);
                            ac.jLabelXRight(-1200, -800, 12, 10, jLabel4);
                            ac.jLabelXRight(-800, -400, 12, 10, jLabel5);
                            ac.jLabelXRight(-400, 0, 12, 10, jLabel6);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel7);
                            ac.jLabelXRight(400, 800, 12, 10, jLabel8);
                            n++; 
                        case 9:
                            Thread.sleep(3000);
                            ac.jLabelXRight(-2000, -1600,12,10, jLabel1);
                            ac.jLabelXRight(-1600, -1200, 12, 10, jLabel2);
                            ac.jLabelXRight(-1200, -800, 12, 10, jLabel3);
                            ac.jLabelXRight(-800, -400, 12, 10, jLabel4);
                            ac.jLabelXRight(-400, 0, 12, 10, jLabel5);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel6);
                            ac.jLabelXRight(400, 800, 12, 10, jLabel7);
                            ac.jLabelXRight(800, 1200, 12, 10, jLabel8);
                            n++;   
                        case 10:
                            Thread.sleep(2000);
                            ac.jLabelXRight(-1600, -1200,12,10, jLabel1);
                            ac.jLabelXRight(-1200, -800, 12, 10, jLabel2);
                            ac.jLabelXRight(-800, -400, 12, 10, jLabel3);
                            ac.jLabelXRight(-400, 0, 12, 10, jLabel4);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel5);
                            ac.jLabelXRight(400, 800, 12, 10, jLabel6);
                            ac.jLabelXRight(800, 1200, 12, 10, jLabel7);
                            ac.jLabelXRight(1200, 1600, 12, 10, jLabel8);
                            n++;
                        case 11:
                            Thread.sleep(3000);
                            ac.jLabelXRight(-1200, -800,12,10, jLabel1);
                            ac.jLabelXRight(-800, -400, 12, 10, jLabel2);
                            ac.jLabelXRight(-400, 0, 12, 10, jLabel3);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel4);
                            ac.jLabelXRight(400, 800, 12, 10, jLabel5);
                            ac.jLabelXRight(800, 1200, 12, 10, jLabel6);
                            ac.jLabelXRight(1200, 1600, 12, 10, jLabel7);
                            ac.jLabelXRight(1600, 2000, 12, 10, jLabel8);
                            n++;
                        case 12:
                            Thread.sleep(2000);
                            ac.jLabelXRight(-800,-400,12,10, jLabel1);
                            ac.jLabelXRight(-400, 0, 12, 10, jLabel2);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel3);
                            ac.jLabelXRight(400, 800, 12, 10, jLabel4);
                            ac.jLabelXRight(800, 1200, 12, 10, jLabel5);
                            ac.jLabelXRight(1200, 1600, 12, 10, jLabel6);
                            ac.jLabelXRight(1600, 2000, 12, 10, jLabel7);
                            ac.jLabelXRight(2000, 2400, 12, 10, jLabel8);
                            n++;
                        case 13:
                            Thread.sleep(3000);
                            ac.jLabelXRight(-400,0,12,10, jLabel1);
                            ac.jLabelXRight(0, 400, 12, 10, jLabel2);
                            ac.jLabelXRight(400, 800, 12, 10, jLabel3);
                            ac.jLabelXRight(800, 1200, 12, 10, jLabel4);
                            ac.jLabelXRight(1200, 1600, 12, 10, jLabel5);
                            ac.jLabelXRight(1600, 2000, 12, 10, jLabel6);
                            ac.jLabelXRight(2000, 2400, 12, 10, jLabel7);
                            ac.jLabelXRight(2400, 2800, 12, 10, jLabel8);
                            n = 0;
                        }
                    }

                }catch(Exception ex)
                {
                    System.out.println(ex);
                }
            }   
    }).start();
}

    public boolean checkConnection(){
        try{
        Process process = java.lang.Runtime.getRuntime().exec("ping www.google.com"); 
        int x = process.waitFor(); 
        if (x == 0) { 
            return true;
        } 
        else { 
            return false;
        }
        }catch(Exception e){
            System.out.println("Ye hai Error : " + e);
        }
        return false;
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Verify = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        Left = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        Music = new javax.swing.JCheckBox();
        F_txtEmailId = new javax.swing.JTextField();
        F_BtnSendOTP = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        F_txtOTP = new javax.swing.JTextField();
        F_BtnVerifyOTP = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        F_txtPasswrod = new javax.swing.JPasswordField();
        jLabel15 = new javax.swing.JLabel();
        F_BtnGoToLogin = new javax.swing.JButton();
        F_txtConfirmPassword = new javax.swing.JPasswordField();
        btnBack = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        Right = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setPreferredSize(new java.awt.Dimension(800, 400));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Left.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel12.setFont(new java.awt.Font("Yu Mincho", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(240, 240, 240));
        jLabel12.setText("Email Id");
        Left.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, -1, -1));

        Music.setBackground(new java.awt.Color(0, 204, 204));
        Music.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Music.setForeground(new java.awt.Color(240, 240, 240));
        Music.setSelected(true);
        Music.setText("Music");
        Music.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MusicActionPerformed(evt);
            }
        });
        Left.add(Music, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 60, -1));

        F_txtEmailId.setBackground(new java.awt.Color(204, 204, 204));
        Left.add(F_txtEmailId, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 70, 160, 30));

        F_BtnSendOTP.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        F_BtnSendOTP.setText("Send OTP");
        F_BtnSendOTP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                F_BtnSendOTPActionPerformed(evt);
            }
        });
        Left.add(F_BtnSendOTP, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, 80, 30));

        jLabel13.setFont(new java.awt.Font("Traditional Arabic", 3, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(240, 240, 240));
        jLabel13.setText("OTP");
        Left.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 40, -1));

        F_txtOTP.setBackground(new java.awt.Color(204, 204, 204));
        F_txtOTP.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        F_txtOTP.setEnabled(false);
        F_txtOTP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                F_txtOTPActionPerformed(evt);
            }
        });
        Left.add(F_txtOTP, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 160, 30));

        F_BtnVerifyOTP.setText("Verify.");
        F_BtnVerifyOTP.setEnabled(false);
        F_BtnVerifyOTP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                F_BtnVerifyOTPActionPerformed(evt);
            }
        });
        Left.add(F_BtnVerifyOTP, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 120, 80, 30));

        jLabel14.setFont(new java.awt.Font("Tempus Sans ITC", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(240, 240, 240));
        jLabel14.setText("New Password");
        Left.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 120, -1));

        F_txtPasswrod.setBackground(new java.awt.Color(102, 255, 153));
        F_txtPasswrod.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        F_txtPasswrod.setEnabled(false);
        Left.add(F_txtPasswrod, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 180, 170, -1));

        jLabel15.setFont(new java.awt.Font("Tempus Sans ITC", 3, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(240, 240, 240));
        jLabel15.setText("Confirm Pasword");
        Left.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, 130, -1));

        F_BtnGoToLogin.setBackground(new java.awt.Color(153, 255, 51));
        F_BtnGoToLogin.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        F_BtnGoToLogin.setForeground(new java.awt.Color(0, 0, 51));
        F_BtnGoToLogin.setText("Go to Login Page..");
        F_BtnGoToLogin.setEnabled(false);
        F_BtnGoToLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                F_BtnGoToLoginActionPerformed(evt);
            }
        });
        Left.add(F_BtnGoToLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, -1, -1));

        F_txtConfirmPassword.setBackground(new java.awt.Color(153, 255, 153));
        F_txtConfirmPassword.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        F_txtConfirmPassword.setEnabled(false);
        Left.add(F_txtConfirmPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 210, 170, -1));

        btnBack.setBackground(new java.awt.Color(51, 51, 0));
        btnBack.setForeground(new java.awt.Color(240, 240, 240));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        Left.add(btnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, 60, -1));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/blank.png"))); // NOI18N
        Left.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        jPanel1.add(Left, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        Right.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/StayHome.png"))); // NOI18N
        jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
        Right.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Checkers.png"))); // NOI18N
        jLabel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 0, 255), 4, true));
        Right.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 400, 400));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Chess.png"))); // NOI18N
        jLabel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 204, 0), 4, true));
        Right.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 0, 400, 400));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Connect4.png"))); // NOI18N
        jLabel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 51, 51), 4, true));
        Right.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 0, 400, 400));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Dot&Box.png"))); // NOI18N
        jLabel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
        Right.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1600, 0, 400, 400));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/HexaPawn.png"))); // NOI18N
        jLabel6.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 102), 4, true));
        Right.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(2000, 0, 400, 400));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Ludo.png"))); // NOI18N
        jLabel7.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
        Right.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(2400, 0, 400, 400));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Nim.png"))); // NOI18N
        jLabel8.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 0), 4, true));
        Right.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(2800, 0, 400, 400));

        jPanel1.add(Right, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 400, 400));

        getContentPane().add(jPanel1, "card2");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    Random rand = new Random();
    int OTP = rand.nextInt(99999999);
    String uName = "";
    private void F_BtnSendOTPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_F_BtnSendOTPActionPerformed
        // TODO add your handling code here:
    if(checkConnection()){ 
        int flag = 1;
        String EmailId = F_txtEmailId.getText();
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if(EmailId.matches(regex))
        {
            F_BtnSendOTP.setText("Sending..");
            try
            {
                // create our mysql database connection
                String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
                String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
                Class.forName(myDriver);
                Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");

                // our SQL SELECT query. 
                // if you only need a few columns, specify them by name instead of using "*"
                String query = "SELECT userName , emailId FROM registration";

                // create the java statement
                Statement st = conn.createStatement();

                // execute the query, and get a java resultset
                ResultSet rs = st.executeQuery(query);

                

                                
                // iterate through the java resultset
                System.out.println("AAya");
                while (rs.next())
                {
                    String Email = rs.getString("emailId");

                    if( Email.equals(EmailId))
                    {
                        uName = rs.getString("userName");
                        flag = 0;
                        break;
                    }
                }
                if(flag != 0)
                {
                    JOptionPane.showMessageDialog(null, "Sorry.. the email is not registered...");
                }
                    st.close();
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception" + e);
            }
            
            if(flag == 0)
            {
                
                String from = "harshkum2k19@gmail.com";
                String username = "harshkum2k19@gmail.com";
                Properties properties = new Properties();
                properties.put("mail.smtp.auth","true");
                properties.put("mail.smtp.starttls.enable","true");
                properties.put("mail.smtp.host","smtp.gmail.com");
                properties.put("mail.smtp.port",587);
                String password = "Harshkumharshkum";
                Session session = Session.getInstance(properties, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
                try {
                    
                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(from));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(EmailId));
                    message.setText("Hi.. " + uName + "       there!!!  the OTP to change password for the Registration No " + uName + " is --->  "   + OTP + " Please verify your emeial by entering the OTP.                           Thank You..... From MCA Family.\n If its not you then kindly forward this message to 'harshkum313@gamil.com' or send whatsapp message regarding this to 8299136658.             \n Thank you from MCA Family.");
                    Transport.send(message);
                    JOptionPane.showMessageDialog(null, "Message Sent successfully...");
                    //F_BtnSendOTP.setEnabled(false);
                    F_BtnSendOTP.setText("Sent");
                    F_txtOTP.setEditable(true);
                    F_txtOTP.setEnabled(true);
                    F_BtnVerifyOTP.setEnabled(true);
                }catch(MessagingException mex)
                {
                    JOptionPane.showMessageDialog(null, "Error message " + mex);
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Please provide the valid email Id.");
            }
        }  
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_F_BtnSendOTPActionPerformed

    
    private void F_BtnVerifyOTPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_F_BtnVerifyOTPActionPerformed
        // TODO add your handling code here:
        if(OTP == Integer.parseInt(F_txtOTP.getText()))
        {
            F_BtnVerifyOTP.setText("Verified");
            F_BtnSendOTP.setEnabled(false);
            F_BtnVerifyOTP.setEnabled(false);
            F_txtEmailId.disable();
            F_txtOTP.disable();
            F_txtPasswrod.enable();
            F_txtConfirmPassword.enable();
            F_BtnGoToLogin.setEnabled(true);
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Sorry.. OTP is not Correct..Please enter the correct OTP");
            F_txtOTP.setText("");
        }
        
    }//GEN-LAST:event_F_BtnVerifyOTPActionPerformed

    private void F_BtnGoToLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_F_BtnGoToLoginActionPerformed
            // TODO add your handling code here:
    //if(checkConnection()){  
    if(true){ 
        if(F_txtPasswrod.getText().equals(F_txtConfirmPassword.getText()))
        {
            try
            {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");
            String query = "update registration set passWord = ? where emailId = ?";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, F_txtPasswrod.getText());
            preparedStmt.setString(2, F_txtEmailId.getText());

            // execute the java preparedstatement
            preparedStmt.executeUpdate();
            conn.close();
            JOptionPane.showMessageDialog(null, "Your Password updated successfully..");
            LogInForm LG = new LogInForm();
            closeMusic();
            setVisible(false);
            LG.setVisible(true);
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exception" + e);
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Sorry.. your Password and Confirm password is not mateched..");
            F_txtPasswrod.setText("");
            F_txtConfirmPassword.setText("");
        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_F_BtnGoToLoginActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        LogInForm Log =  new LogInForm();
        closeMusic();
        n = -1;
        setVisible(false);
        Log.setVisible(true);
        
    }//GEN-LAST:event_btnBackActionPerformed

    private void F_txtOTPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_F_txtOTPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_F_txtOTPActionPerformed

    private void MusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MusicActionPerformed
        // TODO add your handling code here:
        if(Music.isSelected() == true){
            playMusic();
        }else{
            stopMusic();
        }
    }//GEN-LAST:event_MusicActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Forget_Password.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Forget_Password.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Forget_Password.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Forget_Password.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new Forget_Password().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton F_BtnGoToLogin;
    private javax.swing.JButton F_BtnSendOTP;
    private javax.swing.JButton F_BtnVerifyOTP;
    private javax.swing.JPasswordField F_txtConfirmPassword;
    private javax.swing.JTextField F_txtEmailId;
    private javax.swing.JTextField F_txtOTP;
    private javax.swing.JPasswordField F_txtPasswrod;
    private javax.swing.JPanel Left;
    private javax.swing.JCheckBox Music;
    private javax.swing.JPanel Right;
    private javax.swing.ButtonGroup Verify;
    private javax.swing.JButton btnBack;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
