
import AppPackage.AnimationClass;
import jaco.mp3.player.MP3Player;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author harsh_000
 */
public class LogInForm extends javax.swing.JFrame {
    AnimationClass ac = new AnimationClass();
    /**
     * Creates new form LogInForm
     */
    public LogInForm(){
        initComponents();
        playMusic();
        slideshow();
    }
    int n = 0;
    public void slideshow()        
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                
                try{
                    while(n!=-1){
                        switch(n){
                            case 0:
                                Thread.sleep(4000);
                                ac.jLabelXLeft(0,-400,12,10, jLabel1);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel2);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel3);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel4);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel5);
                                ac.jLabelXLeft(2000, 1600, 12, 10, jLabel6);
                                ac.jLabelXLeft(2400, 2000, 12, 10, jLabel7);
                                ac.jLabelXLeft(2800, 2400, 12, 10, jLabel8);
                                n++;
                            case 1:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-400,-800,12,10, jLabel1);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel2);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel3);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel4);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel5);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel6);
                                ac.jLabelXLeft(2000, 1600, 12, 10, jLabel7);
                                ac.jLabelXLeft(2400, 2000, 12, 10, jLabel8);
                                n++;
                            case 2:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-800, -1200,12,10, jLabel1);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel2);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel3);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel4);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel5);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel6);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel7);
                                ac.jLabelXLeft(2000, 1600, 12, 10, jLabel8);
                                n++;
                            case 3:
                                Thread.sleep(3000);
                                ac.jLabelXLeft(-1200, -1600,12,10, jLabel1);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel2);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel3);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel4);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel5);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel6);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel7);
                                ac.jLabelXLeft(1600, 1200, 12, 10, jLabel8);
                                n++;
                            case 4:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-1600, -2000,12,10, jLabel1);
                                ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel2);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel3);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel4);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel5);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel6);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel7);
                                ac.jLabelXLeft(1200, 800, 12, 10, jLabel8);
                                n++;
                            case 5:
                                Thread.sleep(3000);
                                ac.jLabelXLeft(-2000, -2400,12,10, jLabel1);
                                ac.jLabelXLeft(-1600, -2000, 12, 10, jLabel2);
                                ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel3);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel4);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel5);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel6);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel7);
                                ac.jLabelXLeft(800, 400, 12, 10, jLabel8);
                                n++; 
                            case 6:
                                Thread.sleep(2000);
                                ac.jLabelXLeft(-2400, -2800,12,10, jLabel1);
                                ac.jLabelXLeft(-2000, -2400, 12, 10, jLabel2);
                                ac.jLabelXLeft(-1600, -2000, 12, 10, jLabel3);
                                ac.jLabelXLeft(-1200, -1600, 12, 10, jLabel4);
                                ac.jLabelXLeft(-800, -1200, 12, 10, jLabel5);
                                ac.jLabelXLeft(-400, -800, 12, 10, jLabel6);
                                ac.jLabelXLeft(0, -400, 12, 10, jLabel7);
                                ac.jLabelXLeft(400, 0, 12, 10, jLabel8);
                                n++;    
                            case 7:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-2800, -2400,12,10,jLabel1);
                                ac.jLabelXRight(-2400,-2000, 12, 10, jLabel2);
                                ac.jLabelXRight(-2000,-1600, 12, 10, jLabel3);
                                ac.jLabelXRight(-1600, -1200,12,10,jLabel4);
                                ac.jLabelXRight(-1200,-800, 12, 10, jLabel5);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel6);
                                ac.jLabelXRight(-400,0, 12, 10, jLabel7);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel8);
                                n++;
                            case 8:
                                Thread.sleep(2000);
                                ac.jLabelXRight(-2400, -2000,12,10, jLabel1);
                                ac.jLabelXRight(-2000, -1600, 12, 10, jLabel2);
                                ac.jLabelXRight(-1600, -1200, 12, 10, jLabel3);
                                ac.jLabelXRight(-1200, -800, 12, 10, jLabel4);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel5);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel6);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel7);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel8);
                                n++; 
                            case 9:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-2000, -1600,12,10, jLabel1);
                                ac.jLabelXRight(-1600, -1200, 12, 10, jLabel2);
                                ac.jLabelXRight(-1200, -800, 12, 10, jLabel3);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel4);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel5);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel6);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel7);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel8);
                                n++;   
                            case 10:
                                Thread.sleep(2000);
                                ac.jLabelXRight(-1600, -1200,12,10, jLabel1);
                                ac.jLabelXRight(-1200, -800, 12, 10, jLabel2);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel3);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel4);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel5);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel6);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel7);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel8);
                                n++;
                            case 11:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-1200, -800,12,10, jLabel1);
                                ac.jLabelXRight(-800, -400, 12, 10, jLabel2);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel3);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel4);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel5);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel6);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel7);
                                ac.jLabelXRight(1600, 2000, 12, 10, jLabel8);
                                n++;
                            case 12:
                                Thread.sleep(2000);
                                ac.jLabelXRight(-800,-400,12,10, jLabel1);
                                ac.jLabelXRight(-400, 0, 12, 10, jLabel2);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel3);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel4);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel5);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel6);
                                ac.jLabelXRight(1600, 2000, 12, 10, jLabel7);
                                ac.jLabelXRight(2000, 2400, 12, 10, jLabel8);
                                n++;
                            case 13:
                                Thread.sleep(3000);
                                ac.jLabelXRight(-400,0,12,10, jLabel1);
                                ac.jLabelXRight(0, 400, 12, 10, jLabel2);
                                ac.jLabelXRight(400, 800, 12, 10, jLabel3);
                                ac.jLabelXRight(800, 1200, 12, 10, jLabel4);
                                ac.jLabelXRight(1200, 1600, 12, 10, jLabel5);
                                ac.jLabelXRight(1600, 2000, 12, 10, jLabel6);
                                ac.jLabelXRight(2000, 2400, 12, 10, jLabel7);
                                ac.jLabelXRight(2400, 2800, 12, 10, jLabel8);
                                n = 0;
                            }
                        }
        
                    }catch(Exception ex)
                    {
                        System.out.println(ex);
                    }
                }   
        }).start();
    }
    
    MP3Player mp3player = new MP3Player(getClass().getResource("StartSound.mp3")); 
    public void playMusic(){
        mp3player.play();
        mp3player.setRepeat(true);
    }
    public void stopMusic(){
        mp3player.pause();
    }
    public void closeMusic(){
        mp3player.stop();
    }
 
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        textUsername = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        Music = new javax.swing.JCheckBox();
        textPassword = new javax.swing.JPasswordField();
        showHideCheckBox = new javax.swing.JCheckBox();
        btnForgetPassword = new javax.swing.JButton();
        btnSignIn = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        btnRegisterNow = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        AdminLogIn = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();

        jToggleButton1.setText("jToggleButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setPreferredSize(new java.awt.Dimension(800, 400));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/StayHome.png"))); // NOI18N
        jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/HexaPawn.png"))); // NOI18N
        jLabel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 0), 4, true));
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 400, 400));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Dot&Box.png"))); // NOI18N
        jLabel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(102, 0, 102), 4, true));
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 0, 400, 400));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Nim.png"))); // NOI18N
        jLabel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 0, 400, 400));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Checkers.png"))); // NOI18N
        jLabel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(102, 255, 0), 4, true));
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1600, 0, 400, 400));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Ludo.png"))); // NOI18N
        jLabel6.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 4, true));
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(2000, 0, 400, 400));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Connect4.png"))); // NOI18N
        jLabel7.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 51), 4, true));
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(2400, 0, 400, 400));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Chess.png"))); // NOI18N
        jLabel8.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 102), 4, true));
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(2800, 0, 400, 400));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Sitka Heading", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("User Login");
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 90, 90, 20));

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setFont(new java.awt.Font("Tempus Sans ITC", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(240, 240, 240));
        jLabel10.setText("Email ID");
        jPanel3.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, -1));
        jPanel3.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 140, -1, 20));

        textUsername.setBackground(new java.awt.Color(102, 255, 204));
        jPanel3.add(textUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 140, 180, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Password");
        jPanel3.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 180, 70, -1));
        jPanel3.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 180, 30, 20));

        Music.setBackground(new java.awt.Color(102, 255, 204));
        Music.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        Music.setForeground(new java.awt.Color(240, 240, 240));
        Music.setSelected(true);
        Music.setText("Music");
        Music.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MusicActionPerformed(evt);
            }
        });
        jPanel3.add(Music, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 60, -1));

        textPassword.setBackground(new java.awt.Color(102, 255, 204));
        jPanel3.add(textPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 180, 180, -1));

        showHideCheckBox.setBackground(new java.awt.Color(204, 204, 204));
        showHideCheckBox.setFont(new java.awt.Font("Tahoma", 2, 10)); // NOI18N
        showHideCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        showHideCheckBox.setText("Show Password");
        showHideCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showHideCheckBoxActionPerformed(evt);
            }
        });
        jPanel3.add(showHideCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, 100, 20));

        btnForgetPassword.setBackground(new java.awt.Color(51, 51, 0));
        btnForgetPassword.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnForgetPassword.setForeground(new java.awt.Color(240, 240, 240));
        btnForgetPassword.setText("Forget Password");
        btnForgetPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnForgetPasswordActionPerformed(evt);
            }
        });
        jPanel3.add(btnForgetPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 230, -1, 20));

        btnSignIn.setBackground(new java.awt.Color(51, 255, 51));
        btnSignIn.setFont(new java.awt.Font("Traditional Arabic", 1, 14)); // NOI18N
        btnSignIn.setForeground(new java.awt.Color(102, 0, 204));
        btnSignIn.setText("Sign In");
        btnSignIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSignInActionPerformed(evt);
            }
        });
        jPanel3.add(btnSignIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 260, -1, -1));

        jLabel15.setForeground(new java.awt.Color(204, 204, 0));
        jLabel15.setText("Don't have account ?");
        jPanel3.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 330, 110, 20));

        btnRegisterNow.setBackground(new java.awt.Color(153, 153, 0));
        btnRegisterNow.setForeground(new java.awt.Color(102, 102, 0));
        btnRegisterNow.setText("Register Now....");
        btnRegisterNow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterNowActionPerformed(evt);
            }
        });
        jPanel3.add(btnRegisterNow, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 330, -1, 20));

        btnExit.setBackground(new java.awt.Color(204, 0, 0));
        btnExit.setForeground(new java.awt.Color(102, 255, 51));
        btnExit.setText("Exit");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        jPanel3.add(btnExit, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 360, 50, 25));
        jPanel3.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(375, 375, 25, 25));

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });
        jPanel3.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, 100, 88));

        AdminLogIn.setBackground(new java.awt.Color(0, 102, 204));
        AdminLogIn.setFont(new java.awt.Font("Trebuchet MS", 3, 11)); // NOI18N
        AdminLogIn.setText("Log in as Admin");
        AdminLogIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AdminLogInActionPerformed(evt);
            }
        });
        jPanel3.add(AdminLogIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 360, 180, -1));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/blank.png"))); // NOI18N
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 400));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 400, 400));

        getContentPane().add(jPanel1, "card2");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnForgetPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnForgetPasswordActionPerformed
        //JOptionPane.showMessageDialog(null, "Forget wala banana hai");
        closeMusic();
        Forget_Password FP = new Forget_Password();
        setVisible(false);
        n = -1;
        FP.setVisible(true);
    }//GEN-LAST:event_btnForgetPasswordActionPerformed

    
    public boolean checkConnection(){
//        try{
//        Process process = java.lang.Runtime.getRuntime().exec("ping www.google.com"); 
//        int x = process.waitFor(); 
//        if (x == 0) { 
//            return true;
//        } 
//        else { 
//            return false;
//        }
//        }catch(Exception e){
//            System.out.println("Ye hai Error : " + e);
//        }
//        return false;
        return true;
    }
    
    private void btnSignInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSignInActionPerformed
    if(checkConnection()){   
        String EmailId = textUsername.getText();
        String PassWord = textPassword.getText();
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if(EmailId.matches(regex))
        {
            try
            {
            // create our mysql database connection
            //String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");

            // our SQL SELECT query. 
            // if you only need a few columns, specify them by name instead of using "*"
            String query = "SELECT userName, emailId , passWord FROM registration";

            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
            int flag = 0;

            // iterate through the java resultset
            while (rs.next())
            {
                String userName = rs.getString("userName");
                String emailID = rs.getString("emailId");
                String passWord = rs.getString("passWord");

                if(EmailId.equals(emailID) && PassWord.equals(passWord))
                {
                    
                    JOptionPane.showMessageDialog(null, "Successfully Log in as " + userName);
                    
                    String qupdate = " update registration set Online = ? where emailId = ?";
                    PreparedStatement preparedStmt = conn.prepareStatement(qupdate);
                    int online = 1;
                    preparedStmt.setInt(1, online);
                    preparedStmt.setString(2, emailID);
                    preparedStmt.executeUpdate();
                    //JOptionPane.showMessageDialog(null, "You are online Now");
                    
                    flag = 1;
                    closeMusic();
                    setVisible(false);
                    
                    new Home_Frame(emailID).setVisible(true);
                    
                }
            }
            if(flag == 0)
            {
                JOptionPane.showMessageDialog(null, "Sorry.. Username or Password is not matched..."); 
            }
                rs.close();
                conn.close();
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(null, "Got an exceptionsss   " + e);
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Please Provide the valid email Id..");
        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
        
    }//GEN-LAST:event_btnSignInActionPerformed

    private void btnRegisterNowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterNowActionPerformed
       // JOptionPane.showMessageDialog(null, "Registration Page abhi uplabdh nahi hai");
        SignUpForm SignUp = new SignUpForm();
        closeMusic();
        n = -1;
        setVisible(false);
        SignUp.setVisible(true);
    }//GEN-LAST:event_btnRegisterNowActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        // TODO add your handling code here:
        System.exit(1);
    }//GEN-LAST:event_btnExitActionPerformed

    private void showHideCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showHideCheckBoxActionPerformed
        // TODO add your handling code here:
        if(showHideCheckBox.isSelected())
        {
            textPassword.setEchoChar((char)0);
        }
        else
        {
            textPassword.setEchoChar('*');
        }
    }//GEN-LAST:event_showHideCheckBoxActionPerformed

    private void AdminLogInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AdminLogInActionPerformed
        // TODO add your handling code here:
        String pass = JOptionPane.showInputDialog(this, "Enter Password");
        if(pass.equals("Hululu.com"))
        {
            JOptionPane.showMessageDialog(null, "Login Successfully.. Aab change kar sakta hai.");
        }else
        {
            JOptionPane.showMessageDialog(null, "Sorry the Password is not correct...");
        }
    }//GEN-LAST:event_AdminLogInActionPerformed

    private void MusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MusicActionPerformed
        // TODO add your handling code here:
        if(Music.isSelected() == true){
            playMusic();
        }else{
            stopMusic();
        }
    }//GEN-LAST:event_MusicActionPerformed

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        // TODO add your handling code here:
        Introducton In = new Introducton();
        In.setVisible(true);
    }//GEN-LAST:event_jLabel17MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LogInForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LogInForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LogInForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LogInForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LogInForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AdminLogIn;
    private javax.swing.JCheckBox Music;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnForgetPassword;
    private javax.swing.JButton btnRegisterNow;
    private javax.swing.JButton btnSignIn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JCheckBox showHideCheckBox;
    private javax.swing.JPasswordField textPassword;
    private javax.swing.JTextField textUsername;
    // End of variables declaration//GEN-END:variables
}
