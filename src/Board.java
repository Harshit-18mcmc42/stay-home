
import jaco.mp3.player.MP3Player;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;


/**
 *
 * @author Harsh
 */

public class Board extends javax.swing.JFrame {
    
    
    // variables

    boolean forAI = false;
    int[] arrayreturn = new int[100];
    int matrixrow = 0;
    int matrixcol = 0;
    int step = 0;
    int[][] arr = new int[289][6];
    int[][] matrix = new int[17][17];
    int a = 0;
    int k = -1;
    int cellsize = 50;
    int scellsize = 15;
    int b = 15;
    int spacing = 5;
    int prevOne = 8;
    int prevTwo = 280;
    JButton btnPlayerOne = new JButton(new ImageIcon(getClass().getResource("one.png")));
    JButton btnPlayerTwo = new JButton(new ImageIcon(getClass().getResource("Two.png")));
    int index1 = 8;
    int index2 = 280;
    int RowForBtn = 0;
    int ColForBtn = 0;
    boolean skip1 = false;
    boolean skip2 = false;
    boolean skip3 = false;
    boolean skip4 = false;
    int playWith = 0;
    int mode = 0;
    int depth = 0;
    int turn = 0;
    int xaxis;
    int yaxis;
    int point = 0;
    int type = 1;
    int count = 0;
    int flag = 0;
    int drow = 16;
    int dcol = 8;
    int drow1 = 0;
    int dcol1 = 8;
    boolean press = true;

    
    int DhomePosition = 8;
    int HhomePosition = 280;
    
    JButton btn1 = new JButton();
    JButton btn2 = new JButton();
    JButton btn3 = new JButton();
    JButton btn4 = new JButton();
    int[] areturn = new int[5];
    int playerOneSticks = 26;
    int playerTwoSticks = 26;
    boolean isAI = false;
    String globalemail = "";
    MP3Player mp3player = new MP3Player(getClass().getResource("GameSound.mp3")); 
    
    
/** #####################  Board Class Constructor  ################# */ 
//    public Board() {
//        playMusic();
//        initComponents();
//    }
    public Board(String email){
        globalemail = email;
        playMusic();
        initComponents();
    }
    public void playMusic(){
        mp3player.play();
        mp3player.setRepeat(true);
    }
    public void stopMusic(){
        mp3player.pause();
    }
    public void closeMusic(){
        mp3player.stop();
    }
/** ################### End of Constructor  ############### */
    
/** ############## Start Function is called when Start Button is Pressed  ######## */    
    
    
    
    
    
    
    public void Start(){

    /** Function Call to initialize the Starting Component */
    
        Initialize();  
        
    /** For Randomizing the Starting Chance between players */
    
        if(Math.random() > 0.6 || playWith == 1){
            btn_Start.setEnabled(false);
            btnPlayerTwo.setEnabled(false);
            turn = 0;
            count = 0;
        }else{
            btn_Start.setEnabled(false);
            btnPlayerOne.setEnabled(false);
            turn = 1;
            count = 0;
        }
        
        
        
        /* Button One Press action (Left Move)  */

        btn1.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(turn % 2 == 0){
                    
                    if(skip1 == false){
                        
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1-2][1]-3,arr[index1-2][2]-5);
                        btnPlayerOne.setEnabled(false);
                        
                        
                        index1 = index1-2;
                        prevOne = index1;
                        
                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1-4][1]-3,arr[index1-4][2]-5);
                        btnPlayerOne.setEnabled(false);

                        
                        index1 = index1-4;
                        skip1 = false;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }

                    }
                    turn = 1;
                    btnPlayerOne.setEnabled(false);
                    btnPlayerTwo.setEnabled(true);
                    if(playWith == 1){
                        PlayingWithAI();
                    }
                }else{
                    
                    if(skip1 == false){
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2-2][1]-3,arr[index2-2][2]-5);

                        btnPlayerTwo.setEnabled(false);
                        index2 = index2-2;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && prevTwo == 8){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2-4][1]-3,arr[index2-4][2]-5);

                        btnPlayerTwo.setEnabled(false);
                        index2 = index2-4;
                        skip1 = false;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && prevTwo == 8){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }
                    turn = 0;
                    btnPlayerOne.setEnabled(true);
                    btnPlayerTwo.setEnabled(false);
                }
                press = true;
            } 
        });
        
        
        /* Button Two Press action (Right Move)  */
        
        btn2.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(turn % 2 == 0){
                    
                    if(skip2 == false){
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1+2][1]-3,arr[index1+2][2]-5);
                        btnPlayerOne.setEnabled(false);

                        index1 = index1+2;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1+4][1]-3,arr[index1+4][2]-5);
                        btnPlayerOne.setEnabled(false);

                        index1 = index1+4;
                        skip2 = false;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }
                        
                    }
                    turn = 1;
                    btnPlayerOne.setEnabled(false);
                    btnPlayerTwo.setEnabled(true);
                    if(playWith == 1){
                        PlayingWithAI();
                    }
                }else{
                    if(skip2 == false){
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2+2][1]-3,arr[index2+2][2]-5);

                        btnPlayerTwo.setEnabled(false); 
                        index2 = index2+2;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && (prevTwo == 2 || prevTwo == 6 || prevTwo == 10 || prevTwo == 14)){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2+4][1]-3,arr[index2+4][2]-5);

                        btnPlayerTwo.setEnabled(false); 
                        index2 = index2+4;
                        skip2 = false;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && (prevTwo == 2 || prevTwo == 6 || prevTwo == 10 || prevTwo == 14)){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }
                    turn = 0;
                    btnPlayerOne.setEnabled(true);
                    btnPlayerTwo.setEnabled(false);
                }
                press = true;
            }
        });
        
        
        /* Button Three Press action (Up Move)  */
        
        btn3.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(turn % 2 == 0){
                    if(skip3 == false){
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1-34][1]-3,arr[index1-34][2]-5);
                        btnPlayerOne.setEnabled(false);

                        index1 = index1-34;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1-68][1]-3,arr[index1-68][2]-5);
                        btnPlayerOne.setEnabled(false);

                        index1 = index1-68;
                        skip3 = false;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }

                    } 
                    turn = 1;
                    btnPlayerOne.setEnabled(false);
                    btnPlayerTwo.setEnabled(true);
                    if(playWith == 1){
                        PlayingWithAI();
                    }
                }else{
                    
                    if(skip3 == false){
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2-34][1]-3,arr[index2-34][2]-5);

                        btnPlayerTwo.setEnabled(false);
                        index2 = index2-34;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && (prevTwo == 2 || prevTwo == 6 || prevTwo == 10 || prevTwo == 14)){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2-68][1]-3,arr[index2-68][2]-5);

                        btnPlayerTwo.setEnabled(false);
                        index2 = index2-68;
                        skip3 = false;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && (prevTwo == 2 || prevTwo == 6 || prevTwo == 10 || prevTwo == 14)){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }
                    turn = 0;
                    btnPlayerOne.setEnabled(true);
                    btnPlayerTwo.setEnabled(false);
                }
                press = true;
            }
        });
        
        
        /* Button Four Press action (Down Move)  */
        
        btn4.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                if(turn % 2 == 0){
                    
                    if(skip4 == false){
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerOne.setLocation(arr[index1+34][1]-3,arr[index1+34][2]-5);
//                        System.out.println("Count : ; : " + count); 

                        btnPlayerOne.setEnabled(false);
                        index1 = index1+34;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        
                        btnPlayerOne.setLocation(arr[index1+68][1]-3,arr[index1+68][2]-5);
                        btnPlayerOne.setEnabled(false);
                        index1 = index1+68;
                        skip4 = false;
                        prevOne = index1;

                        if(mode == 0 && prevOne / 17 == 16){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(5);
                        }
                        if(mode == 1 && (prevOne == 274 || prevOne == 278 || prevOne == 282 || prevOne == 286)){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(10);
                        }
                        if(mode == 2 && prevOne == HhomePosition){
                            JOptionPane.showMessageDialog(null, "Player One won..");
                            UpdateScore(20);
                        }
                    }
                    turn = 1;
                    btnPlayerOne.setEnabled(false);
                    btnPlayerTwo.setEnabled(true);
                    if(playWith == 1){
                        PlayingWithAI();
                    }
                }else{
                    
                    if(skip4 == false){
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2+34][1]-3,arr[index2+34][2]-5);

                        btnPlayerTwo.setEnabled(false);
                        index2 = index2+34;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && (prevTwo == 2 || prevTwo == 6 || prevTwo == 10 || prevTwo == 14)){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }else{
                        
                        btn1.setVisible(false);
                        btn2.setVisible(false);
                        btn3.setVisible(false);
                        btn4.setVisible(false);
                        btnPlayerTwo.setLocation(arr[index2+68][1]-3,arr[index2+68][2]-5);

                        btnPlayerTwo.setEnabled(false);
                        index2 = index2+68;
                        skip4 = false;
                        prevTwo = index2;
                        
                        if(mode == 0 && prevTwo / 17 == 0){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 1 && (prevTwo == 2 || prevTwo == 6 || prevTwo == 10 || prevTwo == 14)){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        if(mode == 2 && prevTwo == DhomePosition){
                            JOptionPane.showMessageDialog(null, "Player Two won..");
                            UpdateScore(0);
                        }
                        
                    }
                    turn = 0;
                    btnPlayerOne.setEnabled(true);
                    btnPlayerTwo.setEnabled(false);
                }
                press = true;
            }
        });

        
        /* Player One Button Press action (User)  */
        
    btnPlayerOne.addActionListener(new java.awt.event.ActionListener() {

        @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnPlayerOne1ActionPerformed(evt);
        }
        private void btnPlayerOne1ActionPerformed(ActionEvent evt) {
            
                press = false;
                if((index1) % 17 != 0 && arr[index1-1][5] != 9){
                    if(index1-2 != prevTwo){
    //                            System.out.println("Can move left");
                        btn1.setSize(50,50);
                        grid_Label.add(btn1);
                        btn1.setLocation(arr[index1-2][1]-3,arr[index1-2][2]-5);
                        btn1.setVisible(true);
                        skip1 = false;
                    }else if(index1-2 == prevTwo && (index1-2) % 17 != 0 && arr[index1-3][5] != 9){
    //                            System.out.println("Can skip left");
                        btn1.setSize(50,50);
                        grid_Label.add(btn1);
                        btn1.setLocation(arr[index1-4][1]-3,arr[index1-4][2]-5);
                        btn1.setVisible(true);
                        skip1 = true;
                    }
                }
                if((index1+1) % 17 != 0 && arr[index1+1][5] != 9){
                    if(index1+2 != prevTwo){
    //                            System.out.println("Can move right");
                        btn2.setSize(50,50);
                        grid_Label.add(btn2);
                        btn2.setLocation(arr[index1+2][1]-3,arr[index1+2][2]-5);
                        btn2.setVisible(true);
                        skip2 = false;
                    }else if(index1+2 == prevTwo && (index1 + 3) % 17 != 0 && arr[index1 + 3][5] != 9){
    //                            System.out.println("Can skip right");
                        btn2.setSize(50,50);
                        grid_Label.add(btn2);
                        btn2.setLocation(arr[index1+4][1]-3,arr[index1+4][2]-5);
                        btn2.setVisible(true);
                        skip2 = true;
                    }
                }
                if(index1 - 34 >= 0 && arr[index1 - 17][5] != 9){
                    if(index1-34 != prevTwo){
    //                            System.out.println("Can move UP");
                        btn3.setSize(50,50);
                        grid_Label.add(btn3);
                        btn3.setLocation(arr[index1-34][1]-3,arr[index1-34][2]-5);
                        btn3.setVisible(true);
                        skip3 = false;
                    }else if(index1 - 34 == prevTwo && (index1-68) > 0 && arr[index1-51][5] != 9){
    //                            System.out.println("Can skip UP");
                        btn3.setSize(50,50);
                        grid_Label.add(btn3);
                        btn3.setLocation(arr[index1-68][1]-3,arr[index1-68][2]-5);
                        btn3.setVisible(true);
                        skip3 = true;
                    }
                }
                if(index1 + 34 <= 289 && arr[index1 + 17][5] != 9){
                    if(index1+34 != prevTwo){
    //                            System.out.println("Can move Down");
                        btn4.setSize(50,50);
                        grid_Label.add(btn4);
                        btn4.setLocation(arr[index1+34][1]-3,arr[index1+34][2]-5);
                        btn4.setVisible(true);
                        skip4 = false;
                    }else if(index1 + 34 == prevTwo && index1 + 68 < 289 && arr[index1 + 51][5] != 9){
    //                            System.out.println("Can skip Down");
                        btn4.setSize(50,50);
                        grid_Label.add(btn4);
                        btn4.setLocation(arr[index1+68][1]-3,arr[index1+68][2]-5);
                        btn4.setVisible(true);
                        skip4 = true;
                    }
                }
            }
    }); 
        
        
        /* Player Two Button Press action (AI)  */

    btnPlayerTwo.addActionListener(new java.awt.event.ActionListener() {
    @Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnPlayerTwo1ActionPerformed(evt);
        }


        private void btnPlayerTwo1ActionPerformed(ActionEvent evt) {
            press = false;
        if((index2) % 17 != 0 && arr[index2-1][5] != 9){
            if(index2-2 != prevOne){
//                            System.out.println("Can move left");
                btn1.setSize(50,50);
                grid_Label.add(btn1);
                btn1.setLocation(arr[index2-2][1]-3,arr[index2-2][2]-5);
                btn1.setVisible(true);
                skip1 = false;
            }else if(index2-2 == prevOne && (index2-2) % 17 != 0 && arr[index2-3][5] != 9){
//                            System.out.println("Can skip left");
                btn1.setSize(50,50);
                grid_Label.add(btn1);
                btn1.setLocation(arr[index2-4][1]-3,arr[index2-4][2]-5);
                btn1.setVisible(true);
                skip1 = true;
            }
        }
        if((index2+1) % 17 != 0 && arr[index2+1][5] != 9){
            if(index2+2 != prevOne){
//                            System.out.println("Can move right");
                btn2.setSize(50,50);
                grid_Label.add(btn2);
                btn2.setLocation(arr[index2+2][1]-3,arr[index2+2][2]-5);
                btn2.setVisible(true);
                skip2 = false;
            }else if(index2+2 == prevOne && (index2 + 3) % 17 != 0 && arr[index2 + 3][5] != 9){
//                            System.out.println("Can skip right");
                btn2.setSize(50,50);
                grid_Label.add(btn2);
                btn2.setLocation(arr[index2+4][1]-3,arr[index2+4][2]-5);
                btn2.setVisible(true);
                skip2 = true;
            }
        }
        if(index2 - 34 >= 0 && arr[index2 - 17][5] != 9){
            if(index2-34 != prevOne){
//                            System.out.println("Can move UP");
                btn3.setSize(50,50);
                grid_Label.add(btn3);
                btn3.setLocation(arr[index2-34][1]-3,arr[index2-34][2]-5);
                btn3.setVisible(true);
                skip3 = false;
            }else if(index2 - 34 == prevOne && (index2-68) > 0 && arr[index2-51][5] != 9){
//                            System.out.println("Can skip UP");
                btn3.setSize(50,50);
                grid_Label.add(btn3);
                btn3.setLocation(arr[index2-68][1]-3,arr[index2-68][2]-5);
                btn3.setVisible(true);
                skip3 = true;
            }
        }
        if(index2 + 34 <= 289 && arr[index2 + 17][5] != 9){
            if(index2+34 != prevOne){
//                            System.out.println("Can move Down");
                btn4.setSize(50,50);
                grid_Label.add(btn4);
                btn4.setLocation(arr[index2+34][1]-3,arr[index2+34][2]-5);
                btn4.setVisible(true);
                skip4 = false;
            }else if(index2 + 34 == prevOne && index2 + 68 < 289 && arr[index2 + 51][5] != 9){
//                            System.out.println("Can skip Down");
                btn4.setSize(50,50);
                grid_Label.add(btn4);
                btn4.setLocation(arr[index2+68][1]-3,arr[index2+68][2]-5);
                btn4.setVisible(true);
                skip4 = true;
            }
        }             
    }
    });  
        
    }
 
    
    
    
    
    
    
/** ####################  End of Start Function  ################## */  
    

    public void UpdateScore(int score){
        int sss = score;
    if(checkConnection()){
        try
        {
            // create our mysql database connection
            String myDriver = "org.apache.derby.jdbc.EmbeddedDriver";
            String myUrl = "jdbc:derby:C:\\AIGame\\ai_game";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "Harshit", "Harsh@123");
            
            String query1 = "SELECT * FROM registration";
            Statement st = conn.createStatement();
            int scr = 0;

            ResultSet rs = st.executeQuery(query1);
            while (rs.next())
            {
                String Email = rs.getString("emailId");

                if( Email.equals(globalemail))
                {
                    scr = rs.getInt("Rank");
                    break;
                }
            }
            rs.close();
            if(score != 0){
                if(playWith == 1){
                    score = score * 10;
                }
                score = score + scr;
                
            }else{
                
                if(playWith == 0){

                    if(mode == 0)
                        score = scr - 2;
                    else if(mode == 1)
                        score = scr - 5;
                    else if(mode == 2)
                        score = scr - 10;
                }else if(playWith == 1){
                    if(mode == 0)
                        score = scr - 20;
                    else if(mode == 1)
                        score = scr - 50;
                    else if(mode == 2)
                        score = scr - 100;
                }
            }
            
            String query = "update registration set Rank = ? where emailId = ?";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setInt(1, score);
            preparedStmt.setString(2, globalemail);

            preparedStmt.executeUpdate();
            
            String query4 = "SELECT * FROM usergameprofile";
            Statement st1 = conn.createStatement();

            int TP = 0, TW = 0, TL = 0;

            ResultSet rs1 = st1.executeQuery(query4);
            while (rs1.next())
            {
                String Email = rs1.getString("Email");
                String GameName = rs1.getString("GameName");

                if( Email.equals(globalemail) && GameName.equals("Stay Home"))
                {
                    TP = rs1.getInt("TotalPlayed");
                    TW = rs1.getInt("TotalWon");
                    TL = rs1.getInt("TotalLost");
//                    System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                    break;
                }
            }
            
            if(sss != 0){
                TP = TP + 1;
                TW = TW + 1;
//                System.out.println("Score hai : " + TP + "   " + TW );
            }else{
                TL = TL + 1;
                TP = TP + 1;
            }
            
            rs1.close();
            
            String query2 = "update usergameprofile set TotalPlayed = ? , TotalWon = ?, TotalLost = ?  where Email = ? AND GameName = ?";
            PreparedStatement preparedStmt2 = conn.prepareStatement(query2);
            
            preparedStmt2.setInt(1, TP);
            preparedStmt2.setInt(2, TW);
            preparedStmt2.setInt(3, TL);
            preparedStmt2.setString(4, globalemail);
            preparedStmt2.setString(5, "Stay Home");
            preparedStmt2.executeUpdate();
            
//            System.out.println("Conp");
            conn.close();
            
            Home_Frame HF = new Home_Frame(globalemail);
            closeMusic();
            setVisible(false);
            HF.setVisible(true);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Got an exception" + e);
        }
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }
    
/** ############### Initialize Function ################# */
    
    public void Initialize(){
        for(int i = 0; i < 17; i++)
        {
            a = 0;
            for(int j = 0; j < 17; j++)
            {
                k++;
                if(i % 2 == 0 && j % 2 == 0)     // Condition for cell
                {
                    arr[k][0] = 1;
                    arr[k][1] = a;
                    arr[k][2] = b;
                    arr[k][3] = a + cellsize - spacing;
                    arr[k][4] = b + cellsize - spacing;
                    if(j == 16){
                        b += cellsize;
                    }
                    a += cellsize;
                }

                else if(i % 2 != 0 && j % 2 != 0)   // Condition for Small(unused) cell
                {
                    arr[k][0] = 2;
                    arr[k][1] = a;
                    arr[k][2] = b;
                    arr[k][3] = a + scellsize - spacing;
                    arr[k][4] = b + scellsize - spacing;
                    a += scellsize;
                }

                else if(i % 2 == 0 && j % 2 != 0)    // Condtion for Vertical wall
                {
                    arr[k][0] = 3;
                    arr[k][1] = a;
                    arr[k][2] = b;
                    arr[k][3] = a + scellsize - spacing;
                    arr[k][4] = b + cellsize - spacing;
                    a += scellsize;
                }

                else   // For horizontal wall
                {
                    arr[k][0] = 4;
                    arr[k][1] = a;
                    arr[k][2] = b;
                    arr[k][3] = a + cellsize - spacing;
                    arr[k][4] = b + scellsize - spacing;
                    if(j == 16){
                        b += scellsize;
                    }
                    a += cellsize;
                }
            }
        }
        
        for(int i = 0; i < 17; i++){
            for(int j = 0; j < 17; j++){
                if(i % 2 != 0 && j % 2 != 0)
                    matrix[i][j] = 9;
                else if(i%2 == 0 && j % 2 == 0)
                    matrix[i][j] = 0;
                else
                    matrix[i][j] = 3;
            }
        }
        
        btnPlayerOne.setSize(50,50);
        btnPlayerOne.setLocation(arr[8][1]-3,arr[8][2]-5);
        grid_Label.add(btnPlayerOne);
        btnPlayerTwo.setSize(50,50);
        btnPlayerTwo.setLocation(arr[280][1]-3,arr[280][2]-5);
        grid_Label.add(btnPlayerTwo);
        btnPlayerOne.setVisible(true);
        btnPlayerTwo.setVisible(true);
        PlayerOneSticks.setText(Integer.toString(playerOneSticks));
        PlayerTwoSticks.setText(Integer.toString(playerTwoSticks));
    }
    
/** ################# End of Initialize Function ########### */

/** ##################  initComponent Function ####################### */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btn_Start = new javax.swing.JButton();
        PlayerOneName = new javax.swing.JLabel();
        PlayerOneIcon = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        InstructionOne = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        PlayWithOption = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        PlayerOneSticks = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        Music = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        LevelOption = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        PlayerTwoSticks = new javax.swing.JLabel();
        PlayerTwoName = new javax.swing.JLabel();
        IconButton2 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        PlayerTwoIcon = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        grid_Panel = new javax.swing.JPanel();
        grid_Label = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 204, 0));
        setForeground(new java.awt.Color(51, 51, 0));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(0, 0, 0));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.setPreferredSize(new java.awt.Dimension(900, 600));

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btn_Start.setBackground(new java.awt.Color(51, 255, 153));
        btn_Start.setText("Start");
        btn_Start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StartActionPerformed(evt);
            }
        });

        PlayerOneName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        PlayerOneName.setText("   Player One");
        PlayerOneName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        PlayerOneIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/one.png"))); // NOI18N
        PlayerOneIcon.setBorder(new javax.swing.border.MatteBorder(null));
        PlayerOneIcon.setPreferredSize(new java.awt.Dimension(50, 50));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel1.setText("Game Instruction.");

        InstructionOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icon.png"))); // NOI18N
        InstructionOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InstructionOneMouseClicked(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel2.setText("Play With");

        PlayWithOption.setFont(new java.awt.Font("Traditional Arabic", 0, 14)); // NOI18N
        PlayWithOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Friend", "AI" }));
        PlayWithOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PlayWithOptionActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel3.setText("Sticks left");

        PlayerOneSticks.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        PlayerOneSticks.setText("0");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel4.setText("Smart Move left");

        jLabel8.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel8.setText("0");

        Music.setSelected(true);
        Music.setText("Music");
        Music.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MusicActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_Start, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PlayerOneName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(InstructionOne, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(PlayWithOption, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(PlayerOneSticks, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel4)))
                        .addGap(0, 12, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Music)
                    .addComponent(PlayerOneIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PlayerOneName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PlayerOneIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Music)
                .addGap(32, 32, 32)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(InstructionOne, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(PlayWithOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PlayerOneSticks, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addGap(37, 37, 37)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_Start, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(0, 255, 0));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton1.setText("Exit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        LevelOption.setFont(new java.awt.Font("Traditional Arabic", 0, 14)); // NOI18N
        LevelOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Easy", "Medium", "Hard" }));
        LevelOption.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LevelOptionActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel5.setText("Level Instruction.");

        PlayerTwoSticks.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        PlayerTwoSticks.setText("0");

        PlayerTwoName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        PlayerTwoName.setText("   Player Two");
        PlayerTwoName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        IconButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icon.png"))); // NOI18N
        IconButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                IconButton2MouseClicked(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel7.setText("Sticks left");

        PlayerTwoIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Two.png"))); // NOI18N
        PlayerTwoIcon.setText("P Icon");
        PlayerTwoIcon.setBorder(new javax.swing.border.MatteBorder(null));
        PlayerTwoIcon.setPreferredSize(new java.awt.Dimension(50, 50));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 3, 14)); // NOI18N
        jLabel6.setText("Choose Level");

        jLabel10.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel10.setText("0");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel9.setText("Smart Move left");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(PlayerTwoSticks, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 15, Short.MAX_VALUE))
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(PlayerTwoName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LevelOption, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(PlayerTwoIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(36, 36, 36))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(IconButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PlayerTwoName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PlayerTwoIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IconButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LevelOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PlayerTwoSticks, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        grid_Panel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        grid_Panel.setMaximumSize(new java.awt.Dimension(614, 596));
        grid_Panel.setMinimumSize(new java.awt.Dimension(614, 596));

        grid_Label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/LevelOne.png"))); // NOI18N
        grid_Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                grid_LabelMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout grid_PanelLayout = new javax.swing.GroupLayout(grid_Panel);
        grid_Panel.setLayout(grid_PanelLayout);
        grid_PanelLayout.setHorizontalGroup(
            grid_PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, grid_PanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(grid_Label, javax.swing.GroupLayout.PREFERRED_SIZE, 581, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );
        grid_PanelLayout.setVerticalGroup(
            grid_PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(grid_Label, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(grid_Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 583, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(grid_Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 880, 620));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

/** ################## End of initComponent Function ####################### */    
    
    
/** ##################### Start Button Action ############### */ 
    
    public boolean checkConnection(){
        try{
        Process process = java.lang.Runtime.getRuntime().exec("ping www.google.com"); 
        int x = process.waitFor(); 
        if (x == 0) { 
            return true;
        } 
        else { 
            return false;
        }
        }catch(Exception e){
            System.out.println("Ye hai Error : " + e);
        }
        return false;
    }
    
    private void btn_StartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StartActionPerformed
    if(checkConnection()){
        Start();
        LevelOption.setEnabled(false);
        PlayWithOption.setEnabled(false);
    }else{
        JOptionPane.showMessageDialog(null, "You are not connected to Internet.. Please Connect to Internet First.");
    }
    }//GEN-LAST:event_btn_StartActionPerformed
 
/** ##################### End of Start Button Action ############### */  
    
    
    
    
    
    
    
    
/** Double Click for Generating Sticks */
    
    
    
    
    
    
    
    private void grid_LabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_grid_LabelMouseClicked
    if(press == true){
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();   
            xaxis = evt.getX();
            yaxis = evt.getY();
            for(int p = 0; p < 289; p++)
            {
                if(((arr[p][1] <= xaxis) && (arr[p][3] >= xaxis)) && ((arr[p][2] <= yaxis) && (arr[p][4]  >= yaxis))){
                    
                    point = p;
                    type = arr[p][0];
                    if(type != 1 && arr[p][5] != 9){
//                        System.out.println("\n turn : " + turn);
                        if(turn == 0){
//                            System.out.println("Count : " + count);
                            arr[p][5] = 9;
                            matrix[p/17][p%17] = 9;
                            
                            int s1row = prevOne / 17;
                            int s1col = prevOne % 17;
                            int start[] = {s1row,s1col};
                            int end[] = {drow,dcol};
                            
                            int s2row = prevTwo / 17;
                            int s2col = prevTwo % 17;
                            int start1[] = {s2row,s2col};
                            int end1[] = {drow1,dcol1};
                            
                            shortestPathBFS(start,end);
                            int Path = areturn[0];
                            
                            shortestPathBFS(start1,end1);
                            int Pathh = areturn[0];
                            

                            if(Path > 0 && Pathh > 0 && playerOneSticks != 0){
                                count++;
                                btnPlayerOne.setEnabled(false);
                                playerOneSticks--;
                                Draw(p,arr[p][0],0);
                                
                            }else{
                                arr[p][5] = 0;
                                matrix[p/17][p%17] = 0;
                            }
                            if(count == 2){
                                turn = 1;
                                count = 0;
                                btnPlayerOne.setEnabled(false);
                                btnPlayerTwo.setEnabled(true);
                                if(playWith == 1){
                                    PlayingWithAI();
                                }
                            }
                            
                            
                        }else{
                            arr[p][5] = 9;
                            matrix[p/17][p%17] = 9;
                            
                            int s2row = prevTwo / 17;
                            int s2col = prevTwo % 17;
                            int start[] = {s2row,s2col};
                            int end[] = {drow1,dcol1};

                            int s1row = prevOne / 17;
                            int s1col = prevOne % 17;
                            int start1[] = {s1row,s1col};
                            int end1[] = {drow,dcol};

                            shortestPathBFS(start,end);
                            int Path = areturn[0];
                            
                            shortestPathBFS(start1,end1);
                            int Pathh = areturn[0];
                            

                            if(Path > 0 && Pathh > 0 && playerTwoSticks != 0){
                                count++;
                                btnPlayerTwo.setEnabled(false);
                                playerTwoSticks--;
                                Draw(p,arr[p][0],1);
                            }else{
                                arr[p][5] = 0;
                                matrix[p/17][p%17] = 0;
                            }
                            if(count == 2){
                                turn = 0;
                                count = 0;
                                btnPlayerOne.setEnabled(true);
                                btnPlayerTwo.setEnabled(false);
                            }
                        }
                    }
                    break;  
                }  
            }    
        }  
    }
    }//GEN-LAST:event_grid_LabelMouseClicked

    
    
   
    
    
    
    
    
    
    
/** End of Double Click Block */
    
    
    

/** Draw Function to Draw the Sticks */
    
    public void Draw(int point, int type,int t){
        if(t == 0 && playerOneSticks >= 0){
            if(type == 3){
                JButton btn = new JButton();
                btn.setSize(15,50);
                btn.setBackground(Color.green);
                grid_Label.add(btn);
                btn.setLocation(arr[point][1]-3,arr[point][2]-5);
                
                btn.setVisible(true);
            }else if(type == 4){
                JButton btn = new JButton();
                btn.setSize(50,15);
                btn.setBackground(Color.green);
                grid_Label.add(btn);
                btn.setLocation(arr[point][1]-3,arr[point][2]-5);
                
                btn.setVisible(true);
            }
            PlayerOneSticks.setText(Integer.toString(playerOneSticks));
        }else if(t == 1 && playerTwoSticks >= 0){
            if(type == 3){
                JButton btn = new JButton();
                btn.setSize(15,50);
                btn.setBackground(Color.yellow);
                grid_Label.add(btn);
                btn.setLocation(arr[point][1]-3,arr[point][2]-5);
                
                btn.setVisible(true);
            }else if(type == 4){
                JButton btn = new JButton();
                btn.setSize(50,15);
                btn.setBackground(Color.yellow);
                grid_Label.add(btn);
                btn.setLocation(arr[point][1]-3,arr[point][2]-5);
                
                btn.setVisible(true);
            }
            PlayerTwoSticks.setText(Integer.toString(playerTwoSticks));
        }
    }

/** End of Draw Function */
    
    
    
/** Exit Button Action Performed */
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Home_Frame HF = new Home_Frame(globalemail);
        closeMusic();
        setVisible(false);
        HF.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

/* End of Exit Button Action */
    
    
   
    
/* Action for Choosing the Level */
    
    private void LevelOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LevelOptionActionPerformed
        // TODO add your handling code here:
        if(LevelOption.getSelectedIndex() == 0){
            mode = 0;
            grid_Label.setIcon(new ImageIcon(getClass().getResource("LevelOne.png")));
        }
        else if(LevelOption.getSelectedIndex() == 1){
            mode = 1;
            grid_Label.setIcon(new ImageIcon(getClass().getResource("LevelTwo.png")));
        }
        else if(LevelOption.getSelectedIndex() == 2){
            mode = 2;
            grid_Label.setIcon(new ImageIcon(getClass().getResource("LevelThree.png")));
        }
    }//GEN-LAST:event_LevelOptionActionPerformed

/* End of Action for Choosing the Level */
        
    
    
    
/* Action for Choosing the Opponent Type */    
    
    private void PlayWithOptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PlayWithOptionActionPerformed
        // TODO add your handling code here:
        if(PlayWithOption.getSelectedIndex() == 0)
            playWith = 0;
        else
            playWith = 1;
    }//GEN-LAST:event_PlayWithOptionActionPerformed

    private void InstructionOneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InstructionOneMouseClicked
        // TODO add your handling code here:
        Gameinstruction GM = new Gameinstruction();
        GM.setVisible(true);
    }//GEN-LAST:event_InstructionOneMouseClicked

    private void IconButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_IconButton2MouseClicked
        // TODO add your handling code here:
        Modeinstruction MI = new Modeinstruction();
        MI.setVisible(true);
    }//GEN-LAST:event_IconButton2MouseClicked

    private void MusicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MusicActionPerformed
        if(Music.isSelected() == true){
            playMusic();
        }else{
            stopMusic();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_MusicActionPerformed

/* End of Action for Choosing the Opponent Type */ 
    
    
/** ################ Main Function Start of the Program #####################  */
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Board.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                //new Board().setVisible(true);
            }
        });
    }
    
/** ################ End of Man Function   ################## */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton IconButton2;
    private javax.swing.JButton InstructionOne;
    private javax.swing.JComboBox<String> LevelOption;
    private javax.swing.JCheckBox Music;
    private javax.swing.JComboBox<String> PlayWithOption;
    private javax.swing.JLabel PlayerOneIcon;
    private javax.swing.JLabel PlayerOneName;
    private javax.swing.JLabel PlayerOneSticks;
    private javax.swing.JLabel PlayerTwoIcon;
    private javax.swing.JLabel PlayerTwoName;
    private javax.swing.JLabel PlayerTwoSticks;
    private javax.swing.JButton btn_Start;
    private javax.swing.JLabel grid_Label;
    private javax.swing.JPanel grid_Panel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    // End of variables declaration//GEN-END:variables

    
/** Code for Search Process */
    
    public class Cell{
        int x;
        int y;
        int dist;
        Cell prev;
        Cell(int x, int y, int dist, Cell prev){
            this.x = x;
            this.y = y;
            this.dist = dist;
            this.prev = prev;
        }
        @Override
        public String toString(){
            return "(" + x + ", "+ y + ")";
        }
    }
    
    public void shortestPathBFS(int [] start, int[] end){
        int sx = start[0],sy = start[1];
        int dx = end[0], dy = end[1];
        
        if(matrix[sx][sy] == 9 || matrix[dx][dy] == 9)
            return ;
        int m = 17;
        int n = 17;
        Cell[][] cells = new Cell[m][n];
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(matrix[i][j] != 9){
                    cells[i][j] = new Cell(i,j,Integer.MAX_VALUE,null);
                }
            }
        }
        LinkedList<Cell> queue = new LinkedList<>();
        Cell src = cells[sx][sy];
        src.dist = 0;
        queue.add(src);
        Cell dest = null;
        Cell p;
        while((p = queue.poll()) != null){
            if(mode == 0){
                if(p.x == dx){
                    dest = p;
                    break;
                }
            }
            if(mode == 1){
                if(p.x == dx && (p.y == 2 || p.y == 6 || p.y == 10 || p.y == 14)){
                    dest = p;
                    break;
                }
            }
            if(mode == 2){
                if(p.x == dx && p.y == 8){
                    dest = p;
                    break;
                }
            }
            // mvoing up
            visit(cells,queue,p.x-1,p.y,p);
            
            // moving down
            visit(cells,queue,p.x+1,p.y,p);
            
            //moving left
            visit(cells,queue,p.x,p.y-1,p);
            
            // moving right
            visit(cells,queue,p.x,p.y+1,p);
        }
        if(dest == null){
            areturn[0] = 0;
            return;
        }else{
            step = 0;
            LinkedList<Cell> path = new LinkedList<>();
            p = dest;
            do{
                step++;
                path.addFirst(p);

            }while((p = p.prev) != null);
            
            areturn[0] = step;
            areturn[1] = path.get(0).x;
            areturn[2] = path.get(0).y;
            areturn[3] = path.get(1).x;
            areturn[4] = path.get(1).y;
            
            if(forAI == true){
                for(int i = 1; i < step; i += 2){
                    arrayreturn[i-1] = path.get(i).x;
                    arrayreturn[i] = path.get(i).y;
                }
            }
            
//            System.out.println(" Yeh hai ... : " + areturn[0]);
  //          System.out.println("The path is : " + path);
        }
    }
    
    
    public void visit(Cell[][] cells, LinkedList<Cell> queue, int x, int y, Cell parent){
        if(x < 0 || x >= cells.length || y < 0 || y >= 17 || cells[x][y] == null){
            return;
        }
        int dist = parent.dist + 1;
        Cell p = cells[x][y];
        if(dist < p.dist){
            p.dist = dist;
            p.prev = parent;
            queue.add(p);
        }
    }
    

    
    private void PlayingWithAI(){

        int s1row = prevOne / 17;
        int s1col = prevOne % 17;
        int start[] = {s1row,s1col};
        int end[] = {drow,dcol};
        shortestPathBFS(start,end);
        int step1 = areturn[0];
        
        int s2row = prevTwo / 17;
        int s2col = prevTwo % 17;
        int start1[] = {s2row,s2col};
        int end1[] = {drow1,dcol1};
        shortestPathBFS(start1,end1);
        int step2 = areturn[0];
        
        if(playerTwoSticks >= 1){
            if(step2 <= step1)
            {
                btnPlayerTwo.doClick();
                if(areturn[1] < areturn[3] && areturn[2] == areturn[4])
                    btn4.doClick();
                else if(areturn[1] > areturn[3] && areturn[2] == areturn[4])
                    btn3.doClick();
                else if(areturn[1] == areturn[3] && areturn[2] < areturn[4])
                    btn2.doClick();
                else if(areturn[1] == areturn[3] && areturn[2] > areturn[4])
                    btn1.doClick(); 
                turn = 0;
                btnPlayerOne.setEnabled(true);
                btnPlayerTwo.setEnabled(false);
            }
            else
            {
                for(int t = 0; t < 2; t++){
                forAI = true;
                int min = 100;
                int blockx = 0;
                int blocky = 0;
                for(int k = 0; k < 1; k++){
                    for(int i = 0; i < 17; i++){
                        for(int j = 0; j < 17; j++){
                            if(matrix[i][j] == 3){
                                matrix[i][j] = 9;
                                
                                shortestPathBFS(start1,end1);
                                step2 = areturn[0];
                                shortestPathBFS(start,end);
                                step1 = areturn[0];
                                
                                if(step1 < min && step2 > 0){
                                    System.out.println(" Step1 : " +step1 + "  Step2 : " + step2);
                                    blockx = i;
                                    blocky = j;
                                    min = step1;
                                    
                                    for(int p = 0; p < step; p += 2){
                                        int onex = arrayreturn[p];
                                        int oney = arrayreturn[p+1];
                                        int s1 = 0,s2 = 0;                              
                                        matrix[i][j] = 3;
                                        matrix[onex][oney] = 9;
                                        //System.out.println("Onex : " + onex + " Oney  : " + oney);
                                        shortestPathBFS(start1,end1);
                                        s2 = areturn[0];
                                        shortestPathBFS(start,end);
                                        s1 = areturn[0];
                                       
                                        //System.out.println("s1 : " + s1 + " S2 : " + s2);
                                        if(s1 > 0 && s2 > 0){
                                            blockx = onex;
                                            blocky = oney;
                                            break;
                                            //System.out.println("blockx : " + blockx + " blocky : " + blocky); 
                                        }
                                            matrix[onex][oney] = 3;
                                        
                                    }
                                }
                                matrix[i][j] = 3;
                            }
                        }
                    }
                    
                    matrix[blockx][blocky] = 9;
                    playerTwoSticks--;
                    btnPlayerTwo.setEnabled(false);
                    int p = (17 * blockx) + blocky; 
                    arr[p][5] = 9; 
                    Draw(p,arr[p][0],1); 
                }
            }
            btnPlayerOne.setEnabled(true);
            btnPlayerTwo.setEnabled(false);
            turn = 0;
            }
        }else{
            btnPlayerTwo.doClick();
            if(areturn[1] < areturn[3] && areturn[2] == areturn[4])
                btn4.doClick();
            else if(areturn[1] > areturn[3] && areturn[2] == areturn[4])
                btn3.doClick();
            else if(areturn[1] == areturn[3] && areturn[2] < areturn[4])
                btn2.doClick();
            else if(areturn[1] == areturn[3] && areturn[2] > areturn[4])
                btn1.doClick(); 
            turn = 0;
            btnPlayerOne.setEnabled(true);
            btnPlayerTwo.setEnabled(false);
        }
    }
/** End of Function to Play with AI */    
}